#include <osgViewer/Viewer>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Canvas>
#include <osgWidget/Label>
#include <osgWidget/ViewerEventHandlers>
#include <iostream>
#include <sstream>
#include "widgetMenu.h"

// Initialze Vector to occupy memory (problems with compiler)
std::vector<WidgetMenu*> WidgetMenu::_brothers;

WidgetMenu::WidgetMenu(const char* label, const char orientation, const char display, const bool keepSize) : ColorLabel(label)
{
	switch(display){
		case('v'):
			_container = new osgWidget::Box(
				std::string("Menu_") + label,
				osgWidget::Box::VERTICAL,
				true
			);
			break;

		case('h'):
			_container = new osgWidget::Box(
				std::string("Menu_") + label,
				osgWidget::Box::HORIZONTAL,
				true
			);
			break;

		default:
			_container = new osgWidget::Box(
				std::string("Menu_") + label,
				osgWidget::Box::VERTICAL,
				true
			);
			break;
	}

    setColor(0.7f, 0.7f, 0.7f, 0.8f);

    _keepRelation = keepSize;

    if(orientation == 'v') _orientation = 0;
	else if(( orientation == 'h')) _orientation = 1;
	else _orientation =  0;

	this->_brothers.push_back(this);
}

void WidgetMenu::hideOtherWindows()
{
	for (int i = 0; i < _brothers.size(); ++i)
	{
		if(_brothers[i] != this)
		{
			_brothers[i]->_container->hide();
		}
	}
}

void WidgetMenu::addLabel(const char* label)
{
	ColorLabel* colorlabel = new ColorLabel(label);

	_container->addWidget(colorlabel);

	_widgetPointer.push_back(colorlabel);

	_container->resize();

}

void WidgetMenu::addLabel(ColorLabel* label)
{
	_container->addWidget(label);

	_widgetPointer.push_back(label);

	_container->resize();

}

ColorLabel* WidgetMenu::getLabelbyIndex(const int index)
{
	if(!_widgetPointer.empty() && _widgetPointer.size() >= index)
	{
		return _widgetPointer.at(index);
	}
	else
	{
		std::cout << "Out of Range or Empty" << std::endl;
		return NULL;
	}
}

void WidgetMenu::managed(osgWidget::WindowManager* wm)
{
	osgWidget::Label::managed(wm);

	wm->addChild(_container.get());

	_container->hide();
}

void WidgetMenu::positioned()
{
	osgWidget::Label::positioned();

	if(_orientation == 0)
	{

		_container->setOrigin(this->getParent()->getX(), this->getParent()->getHeight());
	}
	else
	{
		_container->setOrigin(this->getParent()->getX()+this->getParent()->getWidth(), this->getParent()->getY());
	}

	if(_keepRelation) _container->resize(getWidth());
	else _container->resize();
}

bool WidgetMenu::mousePush(double, double, const osgWidget::WindowManager*)
{
	 if(!_container->isVisible()) _container->show();

    else _container->hide();

    this->hideOtherWindows();

    return true;
}

bool WidgetMenu::mouseLeave(double, double, const osgWidget::WindowManager*)
{
    if(!_container->isVisible()) setColor(0.8f, 0.8f, 0.8f, 0.8f);

    return true;
}

WidgetMenu::~WidgetMenu(){};