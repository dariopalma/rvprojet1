/**********************************************************************************/
/*                                    Main menu                                   */
/*                               /autor dariopalma                                */
/**********************************************************************************/

#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Canvas>
#include <osgWidget/Label>
#include <osgWidget/ViewerEventHandlers>
#include <iostream>
#include <sstream>
#include <osg/Group>
#include "colorLabel.h"
#include "widgetMenu.h"

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Texture2D>
#include <osg/Depth>
#include <osg/PositionAttitudeTransform>

/* Bit mask necessary for the WindowManager class,
   the projection over the model should to ilustrate should 
   have a corresponding mask: */
// const unsigned int MASK_3D = 0x0f000000;
const unsigned int MASK_2D = 0xf0000000;

osg::ref_ptr<osg::Geode> createBackGround(unsigned int windowX, unsigned int windowY){

	osg::ref_ptr<osg::Geode> geode = new osg::Geode;
	osg::ref_ptr<osg::Texture2D> texture = new osg::Texture2D;
	osg::ref_ptr<osg::Image> image = osgDB::readImageFile("background.png");
	texture->setImage( image.get());

	osg::ref_ptr<osg::Drawable> quad = osg::createTexturedQuadGeometry(
		osg::Vec3(0,0,0), osg::Vec3(windowX,0.0,0.0), osg::Vec3(0.0,windowY,0.0));

	quad->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture.get());
	geode->addDrawable( quad.get() );

	return geode;
}


/// Main Menu example
/** The overall structure is to have a WindowManager to handle the widgets, dimensions,etc.
    In order to create a menu with subwidgets we should add the WidgetMenu and
    then add each label or other widget to be contained by the class.
*/
int main()
{

	unsigned int windowX; // Window Width  
	unsigned int windowY; // Window Height

	osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
	wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0),windowX,windowY);

	osgViewer::Viewer viewer; // Our viewer that contains the camera

	osg::ref_ptr<osg::Group> root = new osg::Group(); // The root of the scene

	osgWidget::WindowManager* wm = new osgWidget::WindowManager(
		&viewer, // The viewer of the scene
		windowX, // Width
		windowY, // Height
		MASK_2D, // Our mask 
		0//osgWidget::WindowManager::WM_PICK_DEBUG // For showing the graphical debugger
	); 

    // We set here the Box or our Window element that will contain the menus
    osgWidget::Window* box = new osgWidget::Box("mainBox",osgWidget::Box::VERTICAL);

    // Here we'll declare two menus, each with its elements 
	WidgetMenu* playerMenu = new WidgetMenu("Start", 'h', 'v', false);

 	playerMenu->addLabel(new StartLabel("Start New Game"));
    playerMenu->addLabel("Load Game");
    playerMenu->addLabel("Erase local content");

    WidgetMenu* settingsMenu = new WidgetMenu("Settings", 'h', 'v', false);

    settingsMenu->addLabel("Adjust Brightness");
    settingsMenu->addLabel("Rendering Options?");

    // Add the Widgets to the Window
    box->addWidget(playerMenu);
    box->addWidget(settingsMenu);

    // We set it's origin at the middle
    box->setOrigin((windowX-box->getWidth())/2,(windowY-box->getHeight())/2);

    // We add everything to our Window Manager
	wm->addChild(box);

	// We can resize our box to cover a certain percentage of the window manager
	// In particular resizePercent(float widthPercentage, float heighPercentage)
	// or just the width
	box->resizePercent();

	// We use the window manager to create a orthonormal camera for our 3D widgets
	osg::Camera* camera = wm->createParentOrthoCamera();
	camera->setCullingActive( false );
	camera->setClearMask( 0 );
	camera->setAllowEventFocus( false );
	camera->setReferenceFrame( osg::Transform::ABSOLUTE_RF );
	camera->setRenderOrder( osg::Camera::POST_RENDER );
	osg::StateSet* ss = camera->getOrCreateStateSet();
	ss->setMode( GL_LIGHTING, osg::StateAttribute::OFF );
	ss->setAttributeAndModes( new osg::Depth(
	osg::Depth::LEQUAL, 1.0, 1.0) );

	/*
	// Here we'll load the Key that can be rotated by the user
	osg::ref_ptr<osg::Node> key = osgDB::readNodeFile("../Models/KeyOld/Worn_Key.obj");
	osg::ref_ptr<osg::PositionAttitudeTransform> pat = new osg::PositionAttitudeTransform;
	//pat->setScale(osg::Vec3(0.2,0.2,0.2));
	pat->addChild(key.get());
	*/

	/*
	camera->setViewMatrixAsLookAt(osg::Vec3(0,-10,0),
								  osg::Vec3(0,0,0),
								  osg::Vec3(0,0,1));
	*/

	camera->addChild(createBackGround(windowX,windowY).get());
	//root->addChild(pat.get());
	root->addChild( camera );

	wm->resizeAllWindows();

	viewer.setSceneData( root.get());
	//viewer.addEventHandler( new osgWidget::KeyboardHandler(wm) );
	viewer.addEventHandler( new osgWidget::MouseHandler(wm) );
	// viewer.addEventHandler( new osgWidget::ResizeHandler(wm , camera) );
	viewer.addEventHandler( new osgWidget::CameraSwitchHandler(wm, camera) );

	return viewer.run();
}