/********************************************************************/
/*                           Task to finish                         */
/********************************************************************/

- Add Input capability to the class
- Add dynamic resizing centering based on childs
- Add File explorer to read Save Files
- Add the style manager
- Add Callback functions by creating subclasses of WidgetMenu
- Add background to main menu
- Finish Local Documentation
- Add subwindow manager