#include <osgWidget/Util>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Canvas>
#include <osgWidget/Label>
#include <osgWidget/ViewerEventHandlers>
#include <iostream>
#include <sstream>

#pragma once

/// ColorLabel class
/**
    A colored Label with basic methods implemented
*/

class ColorLabel : public osgWidget::Label
{

public:

	/// ColorLabel constructor assigns name and default parameters
	/**
	    \param label to be displayed
	*/
	ColorLabel(const char* label);

	/// @overload Handle the mousePush Event
	bool mousePush(double, double, const osgWidget::WindowManager*);

	/// @overload Handle the mouseEnter Event
	bool mouseEnter(double, double, const osgWidget::WindowManager*);

	/// @overload Handle the mouseLeave Event
	bool mouseLeave(double, double, const osgWidget::WindowManager*);

	/// Add size to the default value
	/**
	    \param size like padding
	*/
	void adjustSize(const double size);

    /// Adjust main background color assuming caracters in white
    /**
        \param Color in Vector form
    */
	void changeMainColor(const osg::Vec4& Color);

};

class StartLabel : public ColorLabel
{

public:

	/// ColorLabel constructor assigns name and default parameters
	/**
	    \param label to be displayed
	*/
	StartLabel(const char* label);

	/// @overload Handle the mousePush Event
	bool mousePush(double, double, const osgWidget::WindowManager*);

	/// @overload Handle the mouseEnter Event
	bool mouseEnter(double, double, const osgWidget::WindowManager*);

	/// @overload Handle the mouseLeave Event
	bool mouseLeave(double, double, const osgWidget::WindowManager*);

};

