#include <osgWidget/Util>
#include <osgViewer/Viewer>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Canvas>
#include <osgWidget/Label>
#include <osgWidget/ViewerEventHandlers>
#include <iostream>
#include <sstream>
#include "colorLabel.h"

#include <cstdlib>

#ifdef win32
#define PATHLEVEL "(cd ../Chambre/Chambre/; ./Cshambre.exe)"
#else
#define PATHLEVEL "(cd ../Chambre/Chambre/; ./chambre)"
#endif

ColorLabel::ColorLabel(const char* label)
{
	osgWidget::Label("","");
	{
		setFont("fonts/Vera.ttf");
		setFontSize(14);
		setFontColor(1.0f, 1.0f, 1.0f, 1.0f);
        setColor(0.3f, 0.3f, 0.3f, 1.0f);
        addHeight(18.0f);
        setCanFill(true);
        setLabel(label);
        setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
	}
}

bool ColorLabel::mousePush(double, double, const osgWidget::WindowManager*)
{
	return true;
}

bool ColorLabel::mouseEnter(double, double, const osgWidget::WindowManager*)
{
	setColor(0.6f, 0.6f, 0.6f, 1.0f);
	return true;
}

bool ColorLabel::mouseLeave(double, double, const osgWidget::WindowManager*)
{
	setColor(0.3f, 0.3f, 0.3f, 1.0f);
	return true;
}

void ColorLabel::adjustSize(const double size)
{
	osgWidget::Label::addHeight( size );
}

void ColorLabel::changeMainColor(const osg::Vec4& Color)
{
	osgWidget::Label::setColor(Color);
}

StartLabel::StartLabel(const char* label) : ColorLabel(label){}

bool StartLabel::mousePush(double, double, const osgWidget::WindowManager*)
{
	int status = std::system(PATHLEVEL);
	exit(0);
	return true;
}

bool StartLabel::mouseEnter(double, double, const osgWidget::WindowManager*)
{
	setColor(0.6f, 0.6f, 0.6f, 1.0f);
	return true;
}

bool StartLabel::mouseLeave(double, double, const osgWidget::WindowManager*)
{
	setColor(0.3f, 0.3f, 0.3f, 1.0f);
	return true;
}