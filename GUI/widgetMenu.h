#include <osgWidget/Label>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Canvas>
#include <osgWidget/Label>
#include <osgWidget/ViewerEventHandlers>
#include "colorLabel.h"

#pragma once

/// WidgetMenu class.
/**
    A handler of a ColorLabel and it's contained widgets
*/

class WidgetMenu : public ColorLabel
{
static std::vector<WidgetMenu*> _brothers;

private:

	/// The widget pointer reference vector
	/** Vector used to store pointers of specific elements in order of display
	    from bottom to top */
	std::vector<ColorLabel*> _widgetPointer;

	/// Keep Size Relation of parent boolean
	/** When set to false the childs will not have the same width */
	bool _keepRelation;

	/// Orientation of children display
	/** The children will either be displayed on top 0, or by the 
	    top right corner of the window 1. More modes can be added */ 
	int _orientation;

	void hideOtherWindows();

public:

	/// The internal container of childs
	/** It can contain any osgWidget element and subclasses */
	osg::ref_ptr<osgWidget::Window> _container;

	/// Widget Menu constructor
	/**
	    \param label as the name to be displayed
	    \param orientation as the box orientation of the container
	    \param display top or right corner display
	    \param keepSize force parent width to childs
	    \return the WidgetMenu empty
	*/
	WidgetMenu(const char* label, const char orientation,const char display, const bool keepSize);

	/// add Color label to our menu
	/**
	    \param label to be displayed
	*/ 
	void addLabel(const char* label);

	void addLabel(ColorLabel* label);

	/// get the labels pointer by number from 0 to N-1
	/**
	    \param index of the widget
	*/
	ColorLabel* getLabelbyIndex(const int index);

	/// @overload when the window is first displayed the contents are hidden.
	/**
	    \param wm the current window manager
	*/
	void managed(osgWidget::WindowManager* wm);

	/// @overload Manage the position of the internal elements
	void positioned();

	/// @overload Handle the mousePush Event  
	bool mousePush(double, double, const osgWidget::WindowManager*);

	/// @overload Handle the mouseLeave Event
	bool mouseLeave(double, double, const osgWidget::WindowManager*);

	/// The destructor
	~WidgetMenu();

};