#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Canvas>
#include <osgWidget/Label>
#include <osgWidget/ViewerEventHandlers>
#include <iostream>
#include <sstream>
#include <osg/Group>


float windowX, windowY;
const unsigned int MASK_2D = 0xf0000000;

extern bool tabPressed (osgWidget::Event& ev);

osgWidget::Label* createLabel( const std::string& name, const std::string& text, float size, const osg::Vec4& Color )
{
	osg::ref_ptr<osgWidget::Label> label = new osgWidget::Label(name);
	label->setLabel( text );
	label->setFont( "fonts/arial.tff" );
	label->setFontSize( size );
	label->setFontColor( 1.0f, 1.0f, 1.0f, 1.0f);
	label->setColor( Color );
	label->addSize( 10.0f, 10.0f);
	label->setCanFill( true );
	return label.release();
}

osgWidget::Window* createMenuTest( float winX,  float winY )
{
	osg::ref_ptr<osgWidget::Box> contents = new osgWidget::Box("main", osgWidget::Box::VERTICAL);
	//osg::ref_ptr<osgWidget::Canvas> contents = new osgWidget::Canvas("contents");
	for (int i = 0; i <10 ; ++i)
	{
		osg::Vec4 color( 0.0f, (float)i/2.0f, 0.0f, 1.0f );
		std::stringstream ss;
		ss << "Contenido " << i;

		osgWidget::Label* content = createLabel( ss.str(), ss.str(), 20.0f, color );
		content->setLayer( osgWidget::Widget::LAYER_MIDDLE, i);
		contents->addWidget( content );

	}
	

	// Our main Box : Window. The second parameter corresponds to the orientation
	//osg::ref_ptr<osgWidget::Box> main = new osgWidget::Box("main", osgWidget::Box::VERTICAL);
	//main->setOrigin( winX, winY );
	//main->attachMoveCallback();
	//main->addWidget( contents->embed() );
	contents->setOrigin( winX, winY );
	contents->attachMoveCallback();

	return contents.release();

}

osgWidget::Window* createButtonAndScroll( float winX, float winY )
{
	osg::ref_ptr<osgWidget::Box> contents = new osgWidget::Box( "main", osgWidget::Box::VERTICAL);

	// Add Label to the top
}

const unsigned int MASK_3D = 0x0F000000;

struct ColorLabel: public osgWidget::Label {
    ColorLabel(const char* label):
    osgWidget::Label("", "") {
        setFont("fonts/Vera.ttf");
        setFontSize(14);
        setFontColor(1.0f, 1.0f, 1.0f, 1.0f);
        setColor(0.3f, 0.3f, 0.3f, 1.0f);
        addHeight(50.0f);
        setCanFill(true);
        setLabel(label);
        setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
    }

    bool mousePush(double, double, const osgWidget::WindowManager*) {
        return true;
    }

    bool mouseEnter(double, double, const osgWidget::WindowManager*) {
        setColor(0.6f, 0.6f, 0.6f, 1.0f);
        
        return true;
    }

    bool mouseLeave(double, double, const osgWidget::WindowManager*) {
        setColor(0.3f, 0.3f, 0.3f, 1.0f);
        
        return true;
    }
};

class ColorLabelMenu: public ColorLabel {
    osg::ref_ptr<osgWidget::Window> _window;

public:
    ColorLabelMenu(const char* label):
    ColorLabel(label) {
        _window = new osgWidget::Box(
            std::string("Menu_") + label,
            osgWidget::Box::VERTICAL,
            true
        );

        _window->addWidget(new ColorLabel("Open Some Stuff"));
        _window->addWidget(new ColorLabel("Do It Now"));
        _window->addWidget(new ColorLabel("Hello, How Are U?"));
        _window->addWidget(new ColorLabel("Hmmm..."));
        _window->addWidget(new ColorLabel("Option 5"));

        _window->resize();

        setColor(0.8f, 0.8f, 0.8f, 0.8f);
    }

    void managed(osgWidget::WindowManager* wm) {
        osgWidget::Label::managed(wm);

        wm->addChild(_window.get());

        _window->hide();
    }

    void positioned() {
        osgWidget::Label::positioned();

		_window->setOrigin(getX()+getWidth(), getY());
        _window->resize(getWidth());
    }

    bool mousePush(double, double, const osgWidget::WindowManager*) {
        if(!_window->isVisible()) _window->show();

        else _window->hide();

        return true;
    }

    bool mouseLeave(double, double, const osgWidget::WindowManager*) {
        if(!_window->isVisible()) setColor(0.8f, 0.8f, 0.8f, 0.8f);

        return true;
    }
};


int main()
{

	windowX = 600.0f;
	windowY = 500.0f;

	osgViewer::Viewer viewer;

	osg::ref_ptr<osg::Group> root = new osg::Group();

	osgWidget::WindowManager* wm = new osgWidget::WindowManager(
		&viewer,
		windowX,
		windowY,
		MASK_2D,
		osgWidget::WindowManager::WM_PICK_DEBUG
	); 

	osgWidget::Window* menu = new osgWidget::Box("menu", osgWidget::Box::HORIZONTAL);


 	menu->addWidget(new ColorLabelMenu("Pick me!"));
    menu->addWidget(new ColorLabelMenu("No, wait, pick me!"));
    menu->addWidget(new ColorLabelMenu("Don't pick them..."));
    menu->addWidget(new ColorLabelMenu("Grarar!?!"));

	wm->addChild(menu);

	menu->resizePercent(100.0f);

	osg::Camera* camera = wm->createParentOrthoCamera();

	root->addChild( camera );

	viewer.setSceneData( root.get());
	viewer.setUpViewInWindow( 50, 50, 600, 300);
	viewer.addEventHandler( new osgWidget::KeyboardHandler(wm) );
	viewer.addEventHandler( new osgWidget::MouseHandler(wm) );
	viewer.addEventHandler( new osgWidget::ResizeHandler(wm , camera) );
	viewer.addEventHandler( new osgWidget::CameraSwitchHandler(wm, camera) );

	/*
	osgViewer::Viewer viewer;
	osg::ref_ptr<osgWidget::WindowManager> wm = new osgWidget::WindowManager(&viewer, 600.0f, 300.0f, 0xf0000000 );
	osg::Camera* camera = wm->createParentOrthoCamera();

	wm->addChild( createMenuTest( 0.0f, 0.0f) );
	wm->resizeAllWindows();

	osg::ref_ptr<osg::Group> root = new osg::Group;

	root->addChild( osgDB::readNodeFile("cow.osg"));
	root->addChild( camera );

	viewer.setSceneData( root.get());
	viewer.setUpViewInWindow( 50, 50, 600, 300);
	viewer.addEventHandler( new osgWidget::KeyboardHandler(wm.get()) );
	viewer.addEventHandler( new osgWidget::MouseHandler(wm.get()) );
	viewer.addEventHandler( new osgWidget::ResizeHandler(wm.get() , camera) );
	viewer.addEventHandler( new osgWidget::CameraSwitchHandler(wm.get(), camera) );
	*/
	return viewer.run();
}