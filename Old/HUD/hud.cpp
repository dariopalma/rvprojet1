#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>

#include <osgViewer/Viewer>
#include <osgViewer/CompositeViewer>

#include <osgGA/TrackballManipulator>
#include <osg/Material>
#include <osg/Geode>

#include <osg/BlendFunc>
#include <osg/Depth>

#include <osg/PolygonOffset>
#include <osg/MatrixTransform>

#include <osg/Camera>
#include <osg/RenderInfo>

#include <osgDB/WriteFile>
#include <osgText/Text>

#include <time.h>
#include <string>
#include <sstream>

#include <osg/PositionAttitudeTransform>

double winX = 1280.0f;
double winY = 1024.0f;


class Timer
{
private:
	unsigned int _limit;

public:
	void start(int limit)
	{
		_limit = (unsigned int) limit;
	}

	int timeLeft()
	{
		return _limit-(int)clock()/CLOCKS_PER_SEC;
	}

	std::string timeLeftString()
	{
		std::stringstream ss;
		int minutes = this->timeLeft()/60;
		int seconds = this->timeLeft()%60;
		ss<< minutes << " : " << seconds;
		return ss.str();
	}

	bool timesUP()
	{
		return _limit > (unsigned int) clock()/CLOCKS_PER_SEC;
	}
};

class TimerCB : public osg::NodeCallback
{
private:
	osgText::Text* _timerText;
	Timer* _timer;

public:
	TimerCB( osgText::Text* text, Timer* timer)
	{
		_timerText = text;
		_timer = timer;
	}
	virtual void operator()(osg::Node* node, osg::NodeVisitor* nv)
	{
		_timerText->setText(_timer->timeLeftString());
		traverse(node,nv);
	}

};




osg::Camera* createHUD(Timer* tm)
{

	// Initialize camera and parameters
	osg::Camera* camera = new osg::Camera;
	camera->setClearMask( GL_DEPTH_BUFFER_BIT );
	camera->setRenderOrder( osg::Camera::POST_RENDER );
	camera->setReferenceFrame( osg::Camera::ABSOLUTE_RF );
	camera->setProjectionMatrix( osg::Matrix::ortho2D(0,winX,0,winY));

	// Set Clock text display
	tm->start(2*60);
	camera->setViewMatrix(osg::Matrix::identity());
	camera->setAllowEventFocus(false);

	osg::ref_ptr<osg::Geode> geode = new osg::Geode();
	osg::ref_ptr<osg::StateSet> statesetG = geode->getOrCreateStateSet();
	statesetG->setMode( GL_LIGHTING, osg::StateAttribute::OFF );

	osg::Vec3 position(winX/2, winY-100.0f, 0.0f);

	osg::ref_ptr<osgText::Text> text = new osgText::Text;
	geode->addDrawable( text.get() );

	text->setText(tm->timeLeftString());
	text->setPosition(position);

	camera->setUpdateCallback(new TimerCB( text.get(), tm));

	camera->addChild( geode.get() );
	

	return camera;
}

osg::Camera* createHUD1()
{

	// Initialize camera and parameters
	osg::Camera* camera = new osg::Camera;
	camera->setClearMask( GL_DEPTH_BUFFER_BIT );
	camera->setRenderOrder( osg::Camera::POST_RENDER );
	camera->setReferenceFrame( osg::Camera::ABSOLUTE_RF );
	//camera->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);
	//camera->setProjectionMatrixAsFrustum(0.0, winX, 0.0, winY, 0.0, 1.0);
	camera->setViewMatrix( osg::Matrixd::identity() );
    camera->setProjectionMatrix( osg::Matrixd::identity() );

	// Set Right Corner model
	osg::ref_ptr<osg::PositionAttitudeTransform> rightModelPAT = new osg::PositionAttitudeTransform;
	rightModelPAT->setScale( osg::Vec3(0.01,0.01,0.01));
	rightModelPAT->setPosition( osg::Vec3(0.7,0.7,-10.0));

	osg::ref_ptr<osg::MatrixTransform> matrixTrans = new osg::MatrixTransform;
	osg::Matrix matrixRotation;
	matrixRotation.makeRotate( 20, osg::Vec3(0.0,0.0,1.0) );

	matrixTrans->setMatrix( matrixRotation );

	osg::ref_ptr<osg::Node> rightModel = osgDB::readNodeFile("livreAvecReliure.obj");

	rightModel->getOrCreateStateSet()->setMode( GL_LIGHTING, osg::StateAttribute::OFF );
	matrixTrans->addChild( rightModel.get() );

	rightModelPAT->addChild( matrixTrans.get() );
	camera->addChild( rightModelPAT.get() );

	return camera;
}


int main()
{

	Timer* clockTimer = new Timer;

	osgViewer::Viewer viewer;

	osg::ref_ptr<osg::Group> root = new osg::Group;

	osgViewer::CompositeViewer compositeViewer;

	//osg::ref_ptr<osg::Node> fileNode = osgDB::readNodeFile("dumptruck.osgt");

	osg::ref_ptr<osg::Node> model = osgDB::readNodeFile("livreAvecReliure.obj");

	osg::ref_ptr<osg::Group> cameras = new osg::Group;

	cameras->addChild(createHUD(clockTimer));
	cameras->addChild(createHUD1());

	root->addChild(model.get());
	root->addChild(cameras.get());

	viewer.setSceneData(root.get());

	return viewer.run();
}
