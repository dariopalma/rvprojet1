#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/Material>
#include <osg/StateSet>
#include <osg/PositionAttitudeTransform>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osg/Texture2D>
#include <osg/TexMat>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/Billboard>
#include <osgViewer/View>
#include <osgText/Text>

#include <string>
#include <iostream>

// Keyboard input
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>
#include <osgGA/TrackballManipulator>

using namespace osg;

class BallCallback : public NodeCallback{
    
private:
    //Positions en vertical et horizontal de la balle
	double _position;
	double _positionX;
	double _radius;
	
	//Position du plane 
	double _planePosition;
	
	//Données de la cinématique
	double _acceleration;
	double _speed;
	double _speedX;
	double _deltaTime;
	
	//Coef. des pertes d'energie
	double _attenuation;

	ref_ptr<osgText::Text> _speedTextDisplay;

public:

	BallCallback(){};

    //Methode pour mettre à jour le texte
	void updateTextDisplay(){
		std::ostringstream sStream;
		sStream << "SpeedZ: "<<_speed<<" m/s"<<std::endl<<"speedX: " <<_speedX<< " m/s"<<std::endl
		<<"Speed Total: "<<sqrt(_speed*_speed+_speedX*_speedX) << " m/s";
		_speedTextDisplay->setText(sStream.str());
	}

    //Initialisation de valeurs
	void setPositionAndParameters(Vec3 spherePosit, double planePos, double density){
		_position = spherePosit[2] - density;
		_positionX = spherePosit[0];
		_planePosition = planePos;
		_radius = density;
	}

    //Initialisation de valeurs de la cinematique
	void setCinematic( double a, double s, double dt, double attenuation, double sX){
		_acceleration = a;
		_speed = s;
		_speedX = sX;
		_deltaTime = dt;
		_attenuation = attenuation;
	}

    //Methode pour afficher le texte
    void setTextDisplay( ref_ptr<osgText::Text> text){
    	_speedTextDisplay = text;
    	updateTextDisplay();
    }

    //Boucle principale pour la cinematique
	virtual void operator()(Node* node, NodeVisitor* nv){

		PositionAttitudeTransform* PAT = dynamic_cast<PositionAttitudeTransform*>( node);
		
		//Si la balle touche le sol, il y a des pertes d'energie
		if(_position + _speed*_deltaTime +_acceleration*_deltaTime*_deltaTime <= _planePosition+_radius){
			_speed = -1*_speed/_attenuation;
			_speedX = _speedX/_attenuation;
		}
		
		//Obtenir les nouvelles positions
		_position += _speed*_deltaTime +_acceleration*_deltaTime*_deltaTime/2;
		_positionX += _speedX*_deltaTime;
		
		//Mettre à jour la vitesse
		_speed += _acceleration*_deltaTime + _acceleration*_deltaTime;
		
		//Mettre à jour le texte
		updateTextDisplay();
		//Placer la balle dans sa nouvelle position
		PAT->setPosition(Vec3(_positionX,0.0,_position));
		
		traverse(node,nv);
		
	}

};

int main(){

	//
	//   Set up
	//

	double ballRadius = 1;
	double planeDensity = 1;
	double gravity = -10; // m/s
	double deltaTime = 0.0001;
	double attenuation = 1.5;
	double initialSpeedZ = 0;
	double initialSpeedX = 5; // Only positive

	Vec3 spherePosition = Vec3(-initialSpeedX*3,0.0,20.0);

	// Create viewer
	osgViewer::Viewer viewer;

	// Create root
	ref_ptr<Group> root (new Group);

	// Create Geodes
	ref_ptr<Geode> ballGeode (new Geode);
	ref_ptr<Geode> planeGeode (new Geode);

	// Our Shapes
	ref_ptr<Box> boxAsPlane (new Box(Vec3(),initialSpeedX*6.0,20.0,planeDensity*2));
	ref_ptr<Sphere> ball (new Sphere(Vec3(),ballRadius));

	// Shape Drawables
	ref_ptr<ShapeDrawable> ballDrawable( new ShapeDrawable(ball.get()));
	ref_ptr<ShapeDrawable> planeDrawable (new ShapeDrawable(boxAsPlane.get()));

	// Colors
	ballDrawable->setColor(Vec4(1.0,0.0,0.0,0.0));
	planeDrawable->setColor(Vec4(0.0,0.0,1.0,0.0));

	// Add Drawables
    ballGeode->addDrawable(ballDrawable.get());
    planeGeode->addDrawable(planeDrawable.get());

    // Create Callback for ball
    ref_ptr<BallCallback> ballcb (new BallCallback);
    ballcb->setPositionAndParameters(spherePosition,planeDensity,ballRadius);
    ballcb->setCinematic(gravity,10,deltaTime,attenuation, 5);

    //
    //    Billboard for text
    //

    ref_ptr<Billboard> billboard (new Billboard);

    // Text for the speed
    ref_ptr<osgText::Text> speedText (new osgText::Text);
    speedText->setPosition(Vec3(2.0,2.0,planeDensity));
    speedText->setColor(Vec4(1,1,0,1));
    speedText->setCharacterSize(1);
    speedText->setAxisAlignment(osgText::Text::SCREEN);
    speedText->setDataVariance(Object::DYNAMIC);

    billboard->addDrawable(speedText);

    // Add text to the callback
    ballcb->setTextDisplay(speedText);

    //
    //    Create PositionAttitudeTransform
    //

    ref_ptr<PositionAttitudeTransform> ballPAT (new PositionAttitudeTransform);
    ballPAT->addChild(ballGeode);
    ballPAT->setPosition(spherePosition);
    ballPAT->setUpdateCallback(ballcb.get());

    // Add information as text
    ballPAT->addChild(billboard);

	//
	//   Scene Creation
	//

	root->addChild(planeGeode);
	root->addChild(ballPAT);

	//
	//   Viewer
	//

    viewer.setSceneData (root.get());
    viewer.getCamera()->setClearColor( Vec4(0.0,0.0,0.0,0.0));

    //
    //   Keyboard Input
    //

    viewer.addEventHandler (new osgViewer::StatsHandler);
    viewer.addEventHandler (new osgViewer::WindowSizeHandler);
    viewer.addEventHandler (new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()));

    return (viewer.run());
}