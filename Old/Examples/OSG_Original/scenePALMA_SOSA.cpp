//=============================================================================
//                               @author Dario Palma, Jose Sosa
//                                     Open Scene Graph
//                                  TP 1 - Scene Originale
//=============================================================================

#include <osgViewer/Viewer>
#include <osg/Material>
#include <osg/StateSet>
#include <osg/PositionAttitudeTransform>
#include <osg/MatrixTransform>
#include <osgDB/ReadFile>
#include <osg/Texture2D>
#include <osg/TexMat>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/AnimationPath>

// Keyboard input
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>

using namespace osg;


class UFOCallback : public NodeCallback{


protected:
	double _angle;

public:

	UFOCallback(): _angle(0.0) {};
	virtual void operator()(Node* node, NodeVisitor* nv){

		MatrixTransform* matrixTrans = dynamic_cast<MatrixTransform*>( node );
		Matrix matrixRotation, matrixTranslate;
		matrixRotation.makeRotate(_angle, Vec3(0.0,0.0,1.0));
		matrixTrans->setMatrix(matrixRotation);
		_angle += 0.0001;
		traverse(node,nv);
	}

};



int main(){


    //
    //    Set up
    //

	// Viewer
	osgViewer::Viewer viewer;

	// Root
	ref_ptr<osg::Group> root (new Group);

	// Load Model
	ref_ptr<Node> cowNode (osgDB::readNodeFile("Cow.3ds"));
	ref_ptr<Node> ufoNode (osgDB::readNodeFile("ufo_top_2.stl"));

	// Data variance
	cowNode->setDataVariance( Object::DYNAMIC);

	// Load terrain
	ref_ptr<Node> terrainNode(osgDB::readNodeFile("Terrain2.3ds"));


    //
    //   Materials
    //
    
    ref_ptr<Material> normalMaterial(new Material);
    normalMaterial->setSpecular(Material::FRONT_AND_BACK, Vec4(0.5,0.5,0.5,1.0));
    normalMaterial->setAmbient(Material::FRONT_AND_BACK, Vec4(1.0,1.0,1.0,1.0));
    normalMaterial->setEmission(Material::FRONT, Vec4(1.0,1.0,1.0,1.0));
    normalMaterial->setDiffuse(Material::FRONT_AND_BACK, Vec4(1.0,1.0,1.0,1.0));
    normalMaterial->setShininess(Material::FRONT, 64.0);

    ref_ptr<Material> brightMaterial(new Material);
    brightMaterial->setSpecular(Material::FRONT_AND_BACK, Vec4(1.0,1.0,1.0,1.0));
    brightMaterial->setAmbient(Material::FRONT_AND_BACK, Vec4(1.0,1.0,1.0,1.0));
    brightMaterial->setEmission(Material::FRONT, Vec4(0.2,0.2,0.2,1.0));
    brightMaterial->setDiffuse(Material::FRONT_AND_BACK, Vec4(1.0,1.0,1.0,1.0));
    brightMaterial->setShininess(Material::FRONT, 64.0);

    // Set materials
    cowNode->getOrCreateStateSet()->setAttribute(normalMaterial);
    ufoNode->getOrCreateStateSet()->setAttribute(brightMaterial);
    terrainNode->getOrCreateStateSet()->setAttribute(normalMaterial);

    //
    // Lights
    //
    
    root->getOrCreateStateSet()->setMode(GL_LIGHT1, StateAttribute::ON);

    ref_ptr<Group> lightGroup (new Group);
    ref_ptr<LightSource> lightSource (new LightSource);
    ref_ptr<Light> ufoLight (new Light);

    // UFO light propierties
    ufoLight->setLightNum(1);
    ufoLight->setPosition( osg::Vec4f(0.0,0.0,60.0,1.0));
	ufoLight->setAmbient( osg::Vec4f(1.0,1.0,1.0,1.0)); // color
	ufoLight->setDiffuse( osg::Vec4f(1.0,1.0,1.0,1.0)); // color
	ufoLight->setSpecular(osg::Vec4(1.0,1.0,1.0,1.0));
	ufoLight->setConstantAttenuation(1.0);
	ufoLight->setDirection( osg::Vec3(0.0,0.0,-1.0));
	ufoLight->setSpotCutoff( 25.0);

    lightSource->setLight(ufoLight.get());

    lightGroup->addChild(lightSource.get());

	//
	//   Matrix transform
	//
    ref_ptr<MatrixTransform> cowScaleMAT (new MatrixTransform);
    ref_ptr<MatrixTransform> UFOmatrixTransform( new MatrixTransform);

    //Matrix cowScaleMatrix;
    //cowScaleMatrix.makeScale(5.0,5.0,5.0);

    //cowScaleMAT->setMatrix(cowScaleMatrix);
	cowScaleMAT->addChild(cowNode.get());
    
	//
	//	Positioning
	//
	ref_ptr<PositionAttitudeTransform> ufoPAT (new PositionAttitudeTransform);
	ref_ptr<PositionAttitudeTransform> cowPAT (new PositionAttitudeTransform);

	ufoPAT->addChild(ufoNode.get());
	ufoPAT->setPosition(Vec3(355.0,-405.0,60.0));

	cowPAT->addChild(cowScaleMAT);
	cowPAT->setPosition(Vec3(0.0,0.0,0.0));
	cowPAT->setScale( Vec3(5.0,5.0,5.0));

    //
    //   Animation Path
    //

    ref_ptr<AnimationPath> path (new AnimationPath);

    // Control points
    AnimationPath::ControlPoint CP0( Vec3(0.0,0.0,0.0), Quat(-0.0,Vec3(0.0,0.0,1.0)));
    AnimationPath::ControlPoint CP1( Vec3(0.0,0.0,4.0), Quat(-PI/2,Vec3(0.0,0.0,1.0)));
    AnimationPath::ControlPoint CP2( Vec3(0.0,0.0,10.0), Quat(-PI,Vec3(0.0,0.0,1.0)));
    AnimationPath::ControlPoint CP3( Vec3(0.0,0.0,4.0), Quat(-PI*3/2,Vec3(0.0,0.0,1.0)));
    AnimationPath::ControlPoint CP4( Vec3(0.0,0.0,0.0), Quat(-PI*2,Vec3(0.0,0.0,1.0)));

    // Insert points
    path->insert(0.0,CP0);
    path->insert(2.0,CP1);
    path->insert(3.0,CP2);
    path->insert(4.0,CP3);
    path->insert(6.0,CP4);

    // Define the callback
    ref_ptr<AnimationPathCallback> APCallBack (new AnimationPathCallback(path.get()));

    // Associate matrix and callback

	UFOmatrixTransform->addChild(ufoPAT.get());

    cowScaleMAT->setUpdateCallback( APCallBack.get());
    UFOmatrixTransform->setUpdateCallback( new UFOCallback);


	//
    //   Scene creation
	//
	root->addChild (UFOmatrixTransform.get());
	root->addChild (cowPAT.get());
	root->addChild (terrainNode.get());
	root->addChild (lightGroup.get());

	//
    //    Camera Managment
	//
    viewer.setSceneData (root.get());
    viewer.getCamera()->setClearColor( Vec4(0.0,0.0,0.0,1.0));

    //
    //    Keyboard Input
	//
    viewer.addEventHandler (new osgViewer::StatsHandler);
    viewer.addEventHandler (new osgViewer::WindowSizeHandler);
    viewer.addEventHandler (new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()));

	return (viewer.run());
}