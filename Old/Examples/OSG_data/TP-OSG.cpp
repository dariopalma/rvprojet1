//=============================================================================
//                               @author Dario Palma, Jose Sosa
//                                     Open Scene Graph
//                                  TP 1 - Découvert OSG
//=============================================================================

#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/Material>
#include <osg/StateSet>
#include <osg/PositionAttitudeTransform>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osg/Texture2D>
#include <osg/TexMat>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/Fog>
#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowMap>
#include <osgParticle/ParticleEffect>
#include <osgParticle/FireEffect>
#include <osg/Billboard>
#include <osgViewer/CompositeViewer>
#include <osgViewer/View>


#include <cstring>
#include <iostream>

// Keyboard input
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>
#include <osgGA/TrackballManipulator>


int main()
{
/* OBJECTS CREATION */

	// Creating the viewer	
	osgViewer::CompositeViewer viewer ;

	// Creating the root node
	osg::ref_ptr<osg::Group> root (new osg::Group);
	
	// The geodes containing our shapes
   	osg::ref_ptr<osg::Geode> myshapegeode1 (new osg::Geode);
   	osg::ref_ptr<osg::Geode> myshapegeode2 (new osg::Geode);
	osg::ref_ptr<osg::Geode> boxGeode1( new osg::Geode);
	osg::ref_ptr<osg::Geode> boxGeode2( new osg::Geode);
	osg::ref_ptr<osg::Geode> boxGeode3( new osg::Geode);
	
	// Our shapes
	osg::ref_ptr<osg::Capsule> myCapsule (new osg::Capsule(osg::Vec3f(),1,2));
	osg::ref_ptr<osg::Cone> myCone (new osg::Cone(osg::Vec3f(),3,3));
	osg::ref_ptr<osg::Box> myBox (new osg::Box(osg::Vec3f(0,0,-10),2));
	osg::ref_ptr<osg::Sphere> mySphere (new osg::Sphere(osg::Vec3f(0,1,-8),1));
	osg::ref_ptr<osg::Cone> myCone2 (new osg::Cone(osg::Vec3f(0,1.5,-8),.5,2));


	// Our shape drawable
	osg::ref_ptr<osg::ShapeDrawable> capsuledrawable (new osg::ShapeDrawable(myCapsule.get()));
	osg::ref_ptr<osg::ShapeDrawable> conedrawable (new osg::ShapeDrawable(myCone.get()));
	osg::ref_ptr<osg::ShapeDrawable> boxdrawable (new osg::ShapeDrawable(myBox.get()));
	osg::ref_ptr<osg::ShapeDrawable> spheredrawable (new osg::ShapeDrawable(mySphere.get()));
	osg::ref_ptr<osg::ShapeDrawable> conedrawable2 (new osg::ShapeDrawable(myCone2.get()));

	// Our colors
	conedrawable->setColor(osg::Vec4 (0,0,0,0));
	boxdrawable->setColor(osg::Vec4(0,0,1,0));

	
/* MATERIALS */

	
	// Material ambient
	osg::ref_ptr<osg::Material> mAmbient (new osg::Material);
	mAmbient->setSpecular(osg::Material::FRONT, osg::Vec4(0.5, 0.5, 0.5, 1.0));
	mAmbient->setAmbient(osg::Material::FRONT,  osg::Vec4(0.5, 0.5, 0.5, 1.0));
	mAmbient->setEmission(osg::Material::FRONT, osg::Vec4(0.0, 0.0, 0.0, 1.0));
	mAmbient->setDiffuse(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
	mAmbient->setShininess(osg::Material::FRONT, 25.0);
	//mAmbient->setColorMode(osg::Material::ColorMode(GL_AMBIENT));

	// Material specular
	osg::ref_ptr<osg::Material> mSpecular (new osg::Material);
	mSpecular->setSpecular(osg::Material::FRONT, osg::Vec4(0.7, 0.7, 0.7, 1.0));
	mSpecular->setAmbient(osg::Material::FRONT,  osg::Vec4(0.2, 0.2, 0.2, 1.0));
	mSpecular->setEmission(osg::Material::FRONT, osg::Vec4(0.0, 0.0, 0.0, 1.0));
	mSpecular->setShininess(osg::Material::FRONT, 64.0);
	mSpecular->setDiffuse(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
	//mSpecular->setColorMode(osg::Material::ColorMode(GL_SPECULAR));

	// Material diffuse
	osg::ref_ptr<osg::Material> mDiffuse (new osg::Material);
	mDiffuse->setSpecular(osg::Material::FRONT, osg::Vec4(0, 0, 0, 1.0));
	mDiffuse->setAmbient(osg::Material::FRONT,  osg::Vec4(0.2, 0.2, 0.2, 1.0));
	mDiffuse->setEmission(osg::Material::FRONT, osg::Vec4(0.5, 0.5, 0.5, 1.0));
	mDiffuse->setDiffuse(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
	mDiffuse->setShininess(osg::Material::FRONT, 25.0);
	//mDiffuse->setColorMode(osg::Material::ColorMode(GL_DIFFUSE));

	// Material emission
	osg::ref_ptr<osg::Material> mEmission (new osg::Material);
	mEmission->setSpecular(osg::Material::FRONT, osg::Vec4(0, 0, 0, 1.0));
	mEmission->setAmbient(osg::Material::FRONT,  osg::Vec4(0.2, 0.2, 0.2, 1.0));
	mEmission->setEmission(osg::Material::FRONT, osg::Vec4(0.7, 0.7, 0.7, 1.0));
	mEmission->setDiffuse(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
	mEmission->setShininess(osg::Material::FRONT, 25.0);
	mEmission->setColorMode(osg::Material::ColorMode(GL_EMISSION));

	// Material Ambient and Diffuse
	osg::ref_ptr<osg::Material> mAmbDif (new osg::Material);
	mAmbDif->setColorMode(osg::Material::ColorMode(GL_AMBIENT_AND_DIFFUSE));

/* STATE SET */

	osg::ref_ptr<osg::StateSet> myConeSS(conedrawable->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> myCone2SS(conedrawable2->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> myBoxSS(boxdrawable->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> myCapsuleSS(capsuledrawable->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> mySphereSS(spheredrawable->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> Box1SS(boxGeode1->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> Box2SS(boxGeode2->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> Box3SS(boxGeode3->getOrCreateStateSet());
	osg::ref_ptr<osg::StateSet> rootSS (root->getOrCreateStateSet());

	// Materials

	myConeSS->setAttribute(mAmbient.get(), osg::StateAttribute::ON);
	myCone2SS->setAttribute(mSpecular.get(), osg::StateAttribute::ON);
	myCapsuleSS->setAttribute(mEmission.get(), osg::StateAttribute::ON);
	mySphereSS->setAttribute(mAmbient.get(), osg::StateAttribute::ON);
	Box1SS->setAttribute(mAmbient.get(), osg::StateAttribute::ON);
	Box2SS->setAttribute(mSpecular.get(), osg::StateAttribute::ON);
	Box3SS->setAttribute(mDiffuse.get(), osg::StateAttribute::ON);

/* POSITIONING */

	// Declaration
	osg::ref_ptr<osg::PositionAttitudeTransform> objectPAT(new osg::PositionAttitudeTransform);
	osg::ref_ptr<osg::PositionAttitudeTransform> PATbox1(new osg::PositionAttitudeTransform);
	osg::ref_ptr<osg::PositionAttitudeTransform> PATbox2(new osg::PositionAttitudeTransform);
	osg::ref_ptr<osg::PositionAttitudeTransform> PATbox3(new osg::PositionAttitudeTransform);
	osg::ref_ptr<osg::PositionAttitudeTransform> PATterrain( new osg::PositionAttitudeTransform);

	//Child adding
	objectPAT->addChild(myshapegeode1.get());
	objectPAT->setPosition(osg::Vec3f(5.0,5.0,5.0));

	PATbox1->addChild(boxGeode1.get());
	PATbox1->setPosition(osg::Vec3f(0.0,3.0,0.0));

	PATbox2->addChild(boxGeode2.get());
	PATbox2->setPosition(osg::Vec3f(0.0,6.0,0.0));
	
	PATbox3->addChild(boxGeode3.get());
	PATbox3->setPosition(osg::Vec3f(0.0,9.0,0.0));

	PATterrain->setPosition(osg::Vec3f(0.0,0.0,-15.0));

/* TERRAIN */

	// Create transformation node
	osg::ref_ptr<osg::MatrixTransform> terrainScaleMAT (new osg::MatrixTransform);

	// Scale matrix
	osg::Matrix terrainScaleMatrix;
	terrainScaleMatrix.makeScale(0.05,0.05,0.05);

	// Loading the terrain node
	osg::ref_ptr<osg::Node> terrainnode(osgDB::readNodeFile("Terrain2.3ds"));

	// Set transformation node parameters
	terrainScaleMAT->addChild(terrainnode.get());
	terrainScaleMAT->setMatrix(terrainScaleMatrix);

	PATterrain->addChild(terrainScaleMAT);

	terrainnode->getOrCreateStateSet()->setAttribute(mAmbient,osg::StateAttribute::ON);

/* OBJECTS CREATION */

	osg::ref_ptr<osg::Node> cowNode(osgDB::readNodeFile("cow.osg"));

	osg::ref_ptr<osg::Node> cessnaNode(osgDB::readNodeFile("cessna.osg"));


/* TEXTURES */ /*================================================================================================================*/

	// Getting the state set of the geode
	osg::ref_ptr<osg::StateSet> textureNodeStateSet(objectPAT->getOrCreateStateSet());

	// Loading texture image object
	osg::ref_ptr<osg::Image> image(osgDB::readImageFile("wood.png"));

	// Bind the image to a 2D texture object
	osg::ref_ptr<osg::Texture2D> tex(new osg::Texture2D());
	tex->setDataVariance(osg::Object::DYNAMIC);
	tex->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	tex->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	tex->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	tex->setImage(image.get());

	// Applying texture on the object
	textureNodeStateSet->setTextureAttributeAndModes(0,tex);

/* LIGHTING */

	// Enable lighting
	rootSS->setMode(GL_LIGHTING, osg::StateAttribute::ON);
	rootSS->setMode(GL_LIGHT0, osg::StateAttribute::OFF);
	rootSS->setMode(GL_LIGHT1, osg::StateAttribute::ON);

	// Create nodes
	osg::ref_ptr<osg::Group> lightGroup (new osg::Group);
	osg::ref_ptr<osg::StateSet> lightSS (lightGroup->getOrCreateStateSet());
	osg::ref_ptr<osg::LightSource> lightSource1 = new osg::LightSource;

	// Create a local light
	osg::ref_ptr<osg::Light> myLight = new osg::Light;
	myLight->setLightNum(1);
	myLight->setPosition( osg::Vec4f(15,15,15,0));
	myLight->setAmbient( osg::Vec4f(0.5,0.5,0.5,0.0)); // color
	myLight->setDiffuse( osg::Vec4f(1,1,1,1)); // color
	myLight->setSpecular(osg::Vec4(1,1,1,1));
	myLight->setConstantAttenuation(1.0);
	myLight->setDirection( osg::Vec3(0,0,0));

	// Set light source parameters
	lightSource1->setLight(myLight.get());
	lightSource1->setStateSetModes(*lightSS,1);

	// Add light to source Groupe
	lightGroup->addChild(lightSource1.get());

	// Light markers: small spheres
	osg::ref_ptr<osg::Sphere> myLightSphere (new osg::Sphere(osg::Vec3f(20,20,20),1));
	osg::ref_ptr<osg::ShapeDrawable> lightSphereDrawable (new osg::ShapeDrawable(myLightSphere.get()));
	osg::ref_ptr<osg::Geode> lightGeode(new osg::Geode);
	lightSphereDrawable->setColor(osg::Vec4f(0,0.5,0.5,1));

	lightGeode->addDrawable(lightSphereDrawable.get());
	lightGroup->addChild(lightGeode.get());

/* SHADOWS */

	// Create elements
	osg::ref_ptr<osgShadow::ShadowedScene> shadowedScene(new osgShadow::ShadowedScene);
	osg::ref_ptr<osgShadow::ShadowMap> shadowMap(new osgShadow::ShadowMap);
	shadowedScene->setShadowTechnique(shadowMap.get());

	shadowedScene->addChild(root.get());
	shadowedScene->addChild(lightSource1.get());

/* FOG */

	// Create element fog
	osg::ref_ptr<osg::Fog> myFog(new osg::Fog);
	myFog->setColor( osg::Vec4(0.5,0.5,0.5,1.0));
	myFog->setMode(osg::Fog::LINEAR);
	myFog->setStart(100.0);
	myFog->setEnd(150.0);

	// Attach to StateSet
	rootSS->setAttributeAndModes(myFog.get(),osg::StateAttribute::ON);

/* PARTICLES */

	// Create particle object
	osg::ref_ptr<osgParticle::FireEffect> fireEffect(new osgParticle::FireEffect(osg::Vec3(0.0,0.0,-15.0),3,3));
	fireEffect->setUpEmitterAndProgram();

// BILLBOARD //

	// Create billboard and associate a drawable element
	osg::ref_ptr<osg::Billboard> billboard(new osg::Billboard);
	billboard->setAxis(osg::Vec3(0.0,1.0,0.0));
	billboard->setNormal(osg::Vec3(0.0,0.0,1.0));
	billboard->setMode(osg::Billboard::POINT_ROT_EYE);
	billboard->addDrawable(boxdrawable.get());

// SCENE GRAPH //

	// Add the shape drawable to the geode
	myshapegeode1->addDrawable(capsuledrawable.get());
	myshapegeode1->addDrawable(conedrawable.get());
	myshapegeode1->addDrawable(boxdrawable.get());
	myshapegeode1->addDrawable(spheredrawable.get());

	myshapegeode2->addDrawable(conedrawable2.get());

	boxGeode1->addDrawable(boxdrawable.get());
	boxGeode2->addDrawable(boxdrawable.get());
	boxGeode3->addDrawable(boxdrawable.get());

	// Add the geode to the scene graph root (Group)
	root->addChild(objectPAT.get());
	root->addChild(myshapegeode2.get());
	root->addChild(PATbox1.get());
	root->addChild(PATbox2.get());
	root->addChild(PATbox3.get());
	root->addChild(PATterrain.get());
	//root->addChild(cowNode.get());
	//root->addChild(cessnaNode.get());
	root->addChild(lightGroup.get());
	root->addChild(fireEffect.get());
	root->addChild(billboard.get());

// Views

	osg::ref_ptr<osgViewer::View> view (new osgViewer::View);
	view->setName("View One");
	viewer.addView(view);

	view->setUpViewOnSingleScreen(0);
	// Set the scene data
	//viewer.setSceneData( root.get() ); // With Fog
	view->setSceneData( shadowedScene.get() ); // With Shadows
	view->setCameraManipulator( new osgGA::TrackballManipulator);


	view->addEventHandler( new osgGA::StateSetManipulator(view->getCamera()->getOrCreateStateSet()) );
	// KEYBOARD INPUT //

 	//Stats Event Handler s key
	//Windows size handler

	// SECOND VIEW

	osgViewer::View* view2 = new osgViewer::View;
	view2->setName("View two");
	viewer.addView(view2);

	view2->setUpViewOnSingleScreen(1);
	view2->setSceneData(shadowedScene.get());
	view2->setCameraManipulator(new osgGA::TrackballManipulator);

	view2->addEventHandler(new osgViewer::StatsHandler);
	// add the handler for doing the picking
	//view2->addEventHandler(new PickHandler());

// START VIEWER //

	//The viewer.run() method starts the threads and the traversals.
	return (viewer.run());
}
