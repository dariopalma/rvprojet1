// Chambre.cpp: define el punto de entrada de la aplicación de consola.
//
#include <iostream>
#include <fstream>
#include <vector>
#include <osgViewer/Viewer>
#include <osg/MatrixTransform>
#include <osg/PositionAttitudeTransform>
#include <osgDB/ReadFile>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>
#include <osg/ShapeDrawable>
#include <osgUtil/IntersectionVisitor>
#include <osg/Texture2D>
#include <osg/Material>
#include <osgText/Text>
#include <osg/Light>
#include <osg/LightSource>
#include "Classes.h"
#include "JeuCode.h"
#include "JeuLivres.h"
#include "JeuLock.h"
#include <time.h>
#include <string>
#include <sstream>

#include <osgViewer/ViewerEventHandlers>

#ifdef WIN32
#define SENSDESPL 0.05
#else
#define SENSDESPL 0.005
#endif

using namespace osg;

//Propriétés globales de camera

class inputStatus
{	
public:
	float _deltaMove,_deltaStrafe,sensDepl,sensTour;
	Vec2d mouse,mouseInit;
	bool rotCam;
	inputStatus():_deltaMove(0),_deltaStrafe(0),sensDepl(SENSDESPL),sensTour(0.001),
		mouse(Vec2d(0,0)),mouseInit(Vec2d(0,0)),rotCam(false) {};
};

class CameraMovement : public osgGA::GUIEventHandler{
protected:
	float _mX,_mY;
	bool selectClick;//variable para solo click sin drag
	osgViewer::Viewer *m_viewer;
	IntersectionSelector *m_selector;
public:
	IntersectionSelector* getInterSelect(){return m_selector;}
	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us)
	{
		ref_ptr<osgUtil::LineSegmentIntersector> ray = new osgUtil::LineSegmentIntersector(osgUtil::Intersector::PROJECTION, ea.getXnormalized(), ea.getYnormalized());
		osgUtil::IntersectionVisitor visitor(ray);
		
		switch(ea.getEventType())
		{
		case osgGA::GUIEventAdapter::KEYDOWN:
			hud->setMessage("");
			switch(ea.getKey())
			{
			case osgGA::GUIEventAdapter::KEY_Down:
				inpStat->_deltaMove=-inpStat->sensDepl;
				//afficherMessage("siiii");
				return false;
				break;
			case osgGA::GUIEventAdapter::KEY_Right:
				inpStat->_deltaStrafe=-inpStat->sensDepl;
				return false;
				break;
			case osgGA::GUIEventAdapter::KEY_Up:
				inpStat->_deltaMove=+inpStat->sensDepl;
				return false;
				break;
			case osgGA::GUIEventAdapter::KEY_Left:
				inpStat->_deltaStrafe=inpStat->sensDepl;
				return false;
				break;
			case osgGA::GUIEventAdapter::KEY_Space:
				m_selector->parcourirInventory(1);
			default :
				//inpStat->qqch=false;
				return false;
			}
			break;
		case osgGA::GUIEventAdapter::KEYUP:
			hud->setMessage("");
			switch(ea.getKey())
			{
			case osgGA::GUIEventAdapter::KEY_Down:
				inpStat->_deltaMove=0;
				//afficherMessage("siiii");
				return false;
				break;
			case osgGA::GUIEventAdapter::KEY_Right:
				inpStat->_deltaStrafe=0;
				return false;
				break;
			case osgGA::GUIEventAdapter::KEY_Up:
				inpStat->_deltaMove=0;
				return false;
				break;
			case osgGA::GUIEventAdapter::KEY_Left:
				inpStat->_deltaStrafe=0;
				return false;
				break;
			default :
				//inpStat->qqch=false;
				return false;
			}
			break;
		case osgGA::GUIEventAdapter::PUSH:
			//parte de seleccion 
			selectClick=true;
			hud->setMessage("");
			//parte de movimiento de camara
			if(ea.getButtonMask()==1<<0)
			{
				inpStat->rotCam = true;
				inpStat->mouseInit.x()=ea.getX();
				inpStat->mouseInit.y()=ea.getY();
				inpStat->mouse.x()=ea.getX();
				inpStat->mouse.y()=ea.getY();
			}
			return false;
			break;
		case osgGA::GUIEventAdapter::DRAG:
			hud->setMessage("");
			if(ea.getButtonMask()==1<<0)
			{
				inpStat->mouse.x()=ea.getX();
				inpStat->mouse.y()=ea.getY();
			}
			selectClick=false;
			return false;
			break;
		case osgGA::GUIEventAdapter::RELEASE:
			hud->setMessage("");
			if(ea.getButtonMask()==1<<0)
			{
				inpStat->rotCam = false;
			}

			m_viewer->getCamera()->accept(visitor);

			if(ray->containsIntersections()&&selectClick)
			{
				osgUtil::LineSegmentIntersector::Intersection intersection = ray->getFirstIntersection();
				if(m_selector->handle(intersection,hud))
				{
					//return true;
				}
			}
			selectClick=false;
			return false;
			break;
		default:
			break;
		}
		return false;
	};
	
	//virtual void accept(osgGA::GUIEventHandlerVisitor& v) {v.visit(*this);};
	CameraMovement(inputStatus* x,osgViewer::Viewer *node, IntersectionSelector *selector, HUD* hudN) :osgGA::GUIEventHandler()
	{
		inpStat = x;
		m_viewer = node;
		m_selector = selector;
		selectClick=false;
		hud = hudN;
	}
protected:
	inputStatus* inpStat;
	HUD* hud;
};

class updateScene : public NodeCallback {
public:
	updateScene(inputStatus* x, objetActuel* inventoryX) : pos(0,0,2),inpStat(x),forwView(1,0,0),upView(0,0,1),rightView(0,1,0),inventory(inventoryX),x(0)
	{
		limit = 2.6;
	};
	virtual void operator()(Node* node, NodeVisitor* nv){
		Camera* cam = dynamic_cast<Camera*> (node);
		if(cam)
		{
			double deltaX = inpStat->_deltaStrafe*rightView.x()+inpStat->_deltaMove*forwView.x();
			double deltaY =	inpStat->_deltaStrafe*rightView.y()+inpStat->_deltaMove*forwView.y();

			if( -limit < pos.x()+deltaX && pos.x()+deltaX < limit)
			{
				pos.x()+=deltaX;				
			}

			if( -limit < pos.y()+deltaY && pos.y()+deltaY < limit)
			{
				pos.y()+=deltaY;	
			}

			if(inpStat->rotCam)
			{
				forwView=Quat((inpStat->mouse.x()-inpStat->mouseInit.x())*inpStat->sensTour,upView,
					(inpStat->mouse.y()-inpStat->mouseInit.y())*inpStat->sensTour,rightView,0,Vec3d(0,0,0))*forwView;
				rightView=Quat((inpStat->mouse.x()-inpStat->mouseInit.x())*inpStat->sensTour,upView)*rightView;
				inpStat->mouseInit.x()=inpStat->mouse.x();
				inpStat->mouseInit.y()=inpStat->mouse.y();
			}
			
		
			cam->setViewMatrixAsLookAt(pos,pos+forwView,upView);

			if(!inventory->isNULL())
			{
				
				x+=.01;
				float scale = .8;
				
				ref_ptr<PositionAttitudeTransform> newPAT = new PositionAttitudeTransform;
					
				Vec3 aux = inventory->objetPrincip()->getPAT()->getScale();
				newPAT->setScale(aux);
				aux = pos + forwView*1.5 + rightView*.6 + (forwView^rightView)*.3;
				newPAT->setPosition(aux);
				if(inventory->objetPrincip()->getType()=="Livre")
				{
					//newPAT->setPosition(pos + forwView*.5 - rightView*2 - (forwView^rightView));
					newPAT->setPosition(pos + forwView*.5 - rightView*2 - (forwView^rightView));
				}
				else
				{
					newPAT->setAttitude(Quat(x,Vec3(0,0,1)));
				}
					inventory->objetPrincip()->repositioner(newPAT);
			}
			
		}
		traverse(node,nv);
	}
protected:
	float x;
	Vec3d pos,forwView,upView,rightView;
	objetActuel* inventory;
	inputStatus* inpStat;
	double limit;
};

ref_ptr<PositionAttitudeTransform> desk(){
	ref_ptr<Geode> shapeGeode (new Geode);
	//ref_ptr<Geode> tiroir1 (new Geode);
	ref_ptr<Geode> tiroir2 (new Geode);
	shapeGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(0,0,1),3,2,.1)));
	shapeGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(1.3,.8,.5),.1,.1,1)));
	shapeGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(-1.3,.8,.5),.1,.1,1)));
	shapeGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(1.3,-.8,.5),.1,.1,1)));
	shapeGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(-1.3,-.8,.5),.1,.1,1)));

	ref_ptr<PositionAttitudeTransform> pos1(new PositionAttitudeTransform);

	ref_ptr<PositionAttitudeTransform> pos2(new PositionAttitudeTransform);

	ref_ptr<PositionAttitudeTransform> posGl(new PositionAttitudeTransform);

	tiroir2->setName("Tiroir2");
	tiroir2->addDrawable(new ShapeDrawable(new Box(Vec3d(-1,0,.8),.05,1.5,.3)));
	tiroir2->addDrawable(new ShapeDrawable(new Box(Vec3d(-.2,0,.8),.05,1.5,.3)));
	tiroir2->addDrawable(new ShapeDrawable(new Box(Vec3d(-.6,0,.675),.8,1.5,.05)));
	tiroir2->addDrawable(new ShapeDrawable(new Box(Vec3d(-.6,.75,.8),.85,.05,.3)));
	tiroir2->addDrawable(new ShapeDrawable(new Box(Vec3d(-.6,-.75,.8),.85,.05,.3)));

	pos1->setPosition(Vec3(0,0,0));
	
	//pos1->addChild(tiroir1);
	pos2->addChild(tiroir2);

	posGl->setAttitude(Quat(PI_2,Vec3(0,0,1)));
	posGl->setPosition(Vec3(4,0,0));

	posGl->addChild(pos1.get());
	posGl->addChild(pos2.get());
	posGl->addChild(shapeGeode.get());
	//shapeGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(-.6,0,.8),1,1.5,.3)));

	ref_ptr<Image> image(osgDB::readImageFile("wood.png"));
	ref_ptr<Texture2D> tex(new Texture2D());
	tex->setDataVariance(osg::Object::DYNAMIC);
	tex->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	tex->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	tex->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	tex->setImage(image.get());

	posGl->getOrCreateStateSet()->setTextureAttributeAndModes(0,tex);

	return posGl.get();
}

ref_ptr<Group> salle(){
	float l=10;
	ref_ptr<Group> grpSalle = new Group;

	ref_ptr<Geode> walls (new Geode);
	walls->addDrawable(new ShapeDrawable(new Box(Vec3d(l/2,0,l/4),0.01,l,l/2)));
	walls->addDrawable(new ShapeDrawable(new Box(Vec3d(-l/2,0,l/4),0.01,l,l/2)));
	walls->addDrawable(new ShapeDrawable(new Box(Vec3d(0,l/2,l/4),l,.01,l/2)));
	walls->addDrawable(new ShapeDrawable(new Box(Vec3d(0,-l/2,l/4),l,.01,l/2)));

	//ShapeDrawable* drawable = dynamic_cast<ShapeDrawable*>(shapeGeode->getDrawable(0));

	ref_ptr<Image> imageWall(osgDB::readImageFile("wallTexture2.jpg"));
	ref_ptr<Texture2D> texWall(new Texture2D());
	texWall->setDataVariance(osg::Object::DYNAMIC);
	texWall->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	texWall->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	texWall->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	texWall->setImage(imageWall.get());
	walls->getOrCreateStateSet()->setTextureAttributeAndModes(0,texWall);

	ref_ptr<Geode> floorG(new Geode);
	floorG->addDrawable(new ShapeDrawable(new Box(Vec3d(0,0,0),l,l,0.01)));
	ref_ptr<Image> imageFloor(osgDB::readImageFile("floor.jpg"));
	ref_ptr<Texture2D> texFloor(new Texture2D());
	texFloor->setDataVariance(osg::Object::DYNAMIC);
	texFloor->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	texFloor->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	texFloor->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	texFloor->setImage(imageFloor.get());
	floorG->getOrCreateStateSet()->setTextureAttributeAndModes(0,texFloor);

	ref_ptr<Geode> ceiling (new Geode);
	ceiling->addDrawable(new ShapeDrawable(new Box(Vec3d(0,0,l/2),l,l,.01)));
	ref_ptr<Image> imageCeiling(osgDB::readImageFile("ceiling.jpg"));
	ref_ptr<Texture2D> texCeiling(new Texture2D());
	texCeiling->setDataVariance(osg::Object::DYNAMIC);
	texCeiling->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	texCeiling->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	texCeiling->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	texCeiling->setImage(imageCeiling.get());
	ceiling->getOrCreateStateSet()->setTextureAttributeAndModes(0,texCeiling);

	std::vector<ref_ptr<PositionAttitudeTransform> > vectPAT;
	vectPAT = chargerFigures("../../Models/fichierChargeStatiques.txt");

	for(int i=0;i<vectPAT.size();i++)
	{
		grpSalle->addChild(vectPAT[i]);
	}
	grpSalle->addChild(walls);
	grpSalle->addChild(floorG);
	grpSalle->addChild(ceiling);
	grpSalle->addChild(desk().get());



	return grpSalle;
}

std::vector<Objet*> objetsInteractifs(){
	std::vector<Objet*> resultat;

	ref_ptr<PositionAttitudeTransform> pos= new PositionAttitudeTransform;
	pos->setPosition(Vec3(4,0,0));
	pos->setAttitude(Quat(PI_2,Vec3(0,0,1)));
	resultat.push_back(new Tiroir("Tiroir1",pos.get()));

	int comptCle = 0,comptLivre = 0;
	std::vector<ref_ptr<PositionAttitudeTransform> > figures = chargerFigures("../../Models/fichierCharge.txt");
	for(int i = 0; i<figures.size() ; i++)
	{
		if(figures[i]->getName()=="Cheminee")
		{
			resultat.push_back(new Cheminee("Cheminee",figures[i].get()));
		}else
		if(figures[i]->getName()=="Pot")
		{
			ref_ptr<Material> material = new Material;
			material->setColorMode(Material::ColorMode(GL_AMBIENT_AND_DIFFUSE));
			material->setDiffuse(Material::FRONT_AND_BACK,Vec4(0.5,0.0,0.0,1.0));
			material->setAmbient(Material::FRONT_AND_BACK,Vec4(0.5,0.1,0.0,1.0));
			figures[i]->getOrCreateStateSet()->setAttribute(material.get(), StateAttribute::ON | StateAttribute::OVERRIDE);
			resultat.push_back(new Pot("Pot",figures[i].get()));
		}else
		if(figures[i]->getName()=="Arbre")
		{
			ref_ptr<Material> material = new Material;
			material->setColorMode(Material::ColorMode(GL_AMBIENT_AND_DIFFUSE));
			material->setDiffuse(Material::FRONT_AND_BACK,Vec4(0.2,0.1,0.0,1.0));
			material->setAmbient(Material::FRONT_AND_BACK,Vec4(0.2,0.1,0.0,1.0));
			figures[i]->getOrCreateStateSet()->setAttribute(material.get(), StateAttribute::ON | StateAttribute::OVERRIDE);
			resultat.push_back(new Arbre("Arbre",figures[i].get()));
		}else
		if(figures[i]->getName()=="Boite")
		{
			resultat.push_back(new Boite("Boite",figures[i].get()));
		}else
		if(figures[i]->getName()=="Lock")
		{
			resultat.push_back(new Lock("Lock",figures[i].get()));
		}else
		if(figures[i]->getName()=="Bombe")
		{
			resultat.push_back(new Bombe("Bombe",figures[i].get()));
		}
	}

	return resultat;
}

ref_ptr<Group> doLighting(Group* root)
{
	root->getOrCreateStateSet()->setMode(GL_LIGHT0, StateAttribute::ON);
	root->getOrCreateStateSet()->setMode(GL_LIGHT1, StateAttribute::ON);
	root->getOrCreateStateSet()->setMode(GL_LIGHT2, StateAttribute::ON);

	ref_ptr<LightSource> lightSource1 = new LightSource;
	ref_ptr<Light> ambientLight = new Light;

    ambientLight->setLightNum(0);
    ambientLight->setPosition( osg::Vec4f(0.0,0.0,4.9,1.0));
	ambientLight->setAmbient( osg::Vec4f(0.7,0.7,0.7,1.0)); // color
	ambientLight->setDiffuse( osg::Vec4f(1.0,1.0,1.0,1.0)); // color
	ambientLight->setSpecular(osg::Vec4(1.0,1.0,1.0,1.0));
	ambientLight->setConstantAttenuation(2.0);
	ambientLight->setDirection( osg::Vec3(0.0,0.0,-1.0));
	ambientLight->setSpotCutoff( 75.0);

    lightSource1->setLight(ambientLight.get());

	ref_ptr<LightSource> lightSource2 = new LightSource;
    ref_ptr<Light> chemineeLight = new Light;

    chemineeLight->setLightNum(1);
    chemineeLight->setPosition( osg::Vec4f(-1.8,4.8,0.2,1.0));
	chemineeLight->setAmbient( osg::Vec4f(1.0,0.3,0.3,1.0)); // color
	chemineeLight->setDiffuse( osg::Vec4f(1.0,0.3,0.3,1.0)); // color
	chemineeLight->setSpecular(osg::Vec4(1.0,0.5,0.5,1.0));
	chemineeLight->setConstantAttenuation(3.0);
	chemineeLight->setQuadraticAttenuation(1.0);

    lightSource2->setLight(chemineeLight.get());

   	ref_ptr<LightSource> lightSource3 = new LightSource;
    ref_ptr<Light>  lampLight = new Light;

    lampLight->setLightNum(2);
    lampLight->setPosition( osg::Vec4f(0.0,0.0,4,1.0));
	lampLight->setAmbient( osg::Vec4f(1.0,1.0,1.0,1.0)); // color
	lampLight->setDiffuse( osg::Vec4f(1.0,1.0,1.0,1.0)); // color
	lampLight->setConstantAttenuation(1.0);
	//lampLight->setQuadraticAttenuation(1.0);
	lampLight->setDirection( osg::Vec3(0.0,0.0,1.0));
	lampLight->setSpotCutoff( 15.0);

	lightSource3->setLight(lampLight.get());

    ref_ptr<Group> lightGroup = new Group;
    lightGroup->addChild(lightSource1.get());
    lightGroup->addChild(lightSource2.get());
    lightGroup->addChild(lightSource3.get());

	return lightGroup;
}

int main(int argc, char* argv[])
{
	srand(time(NULL));
	osgViewer::Viewer viewer;
	ref_ptr<Group> root (new Group);

	//Salle avec des objets statiques
	root->addChild(salle());

	//////Création des PAT pour chaque figure
	HUD* hud = new HUD;
	inputStatus* gh= new inputStatus;
	JeuCode* jeu = new JeuCode;

	BrickGame* brick = new BrickGame();
	JeuLivres* livres = new JeuLivres;
	JeuLock* jeuLock = new JeuLock();
	JeuBombe* jeuB = new JeuBombe(jeu->getIDbonneCle());
	
	root->addChild(jeuLock->getFigure());

	std::vector<Objet*> objets = objetsInteractifs();

	//Pour le jeu de la bombe
	std::vector<Objet*> objJeuBombe = jeuB->getObjets();
	for(int i = 0; i<3; i++)
	{
		objets.push_back(objJeuBombe[i]);
	}

	//Pour le Jeu du code
	std::vector<Objet*> objJeuCode = jeu->getClesEtKeyholes();
	for(int i = 0; i<21; i++)
	{
		objets.push_back(objJeuCode[i]);
	}

	//Pour le Jeu des Livres
	std::vector<Objet*> objJeuLivres = livres->getLivres();
	for(int i = 0; i<objJeuLivres.size(); i++)
	{
		objets.push_back(objJeuLivres[i]);
	}

	objets.push_back(brick->getBrick());

	for(int i=0;i<objets.size();i++)
	{
		root->addChild(objets[i]->getPAT());
	}

	ref_ptr <CameraMovement> monPremier (new CameraMovement(gh,&viewer,new IntersectionSelector(objets),hud));
	viewer.addEventHandler(monPremier.get());
	updateScene* help = new updateScene(gh,monPremier->getInterSelect()->getInventory());
	viewer.getCamera()->setUpdateCallback(help);

	root->addChild(jeu->getFigure());
	root->addChild(jeuB->getGroup());
	root->addChild(brick->getFigure());
	//root->addChild(doLighting(root.get()).get());
	root->addChild(hud->createHUD());


	ref_ptr<Material> rootMaterial = new Material;
	rootMaterial->setColorMode(Material::ColorMode(GL_AMBIENT_AND_DIFFUSE));
	root->getOrCreateStateSet()->setAttribute(rootMaterial.get(), StateAttribute::ON);
	
	viewer.setSceneData(root.get());
	viewer.realize();
	viewer.addEventHandler(new osgViewer::StatsHandler);
	while(!viewer.done())
	{
		viewer.frame();
	}
	
	return 0;
}

