#pragma once
#include "Classes.h"

class JeuCode
{
private:
	char code[3],tentative[3];
	ref_ptr<Group> jeu;
	Cle* cles[7];
	KeyHoles* keyHoles[7];
	Bouton* bouton[7];
	bool correct;
	ref_ptr<Geode> leds[3];
public:
	JeuCode();
	bool tentativeRempli();
	void resetLEDS();
	Objet* getObjet(char type, char num);
	void allumerLeds(int fix,int pij); //verdes, malas
	bool codeTrouve(){return correct;}
	void resetTentatives();
	void ajouterNumero(int i);
	int getIDbonneCle();
	std::vector<Objet*> getClesEtKeyholes();
	void evaluer();
	void resetJeu();
	ref_ptr<Group> getFigure(){return jeu;}
};
