#include "JeuLock.h"

JeuLock::JeuLock(){

	fig = new PositionAttitudeTransform;
	couv = new PositionAttitudeTransform;

	code[0] = '7';
	code[1] = '1';
	code[2] = '6';
	code[3] = '3';
	code[4] = '4';

	ref_ptr<Group> group;
	ref_ptr<Geode> couverture(new Geode);
	couverture->addDrawable(new ShapeDrawable(new Box(Vec3d(0,0,1),1.5,2.5,.08)));

	ref_ptr<Geode> boite (new Geode);
	boite->setName("Boite");
	boite->addDrawable(new ShapeDrawable(new Box(Vec3d(-.75,0,.45),.08,2.5,1)));
	boite->addDrawable(new ShapeDrawable(new Box(Vec3d(.75,0,.45),.08,2.5,1)));
	boite->addDrawable(new ShapeDrawable(new Box(Vec3d(0,0,0),1.5,2.5,.08)));
	boite->addDrawable(new ShapeDrawable(new Box(Vec3d(0,1.21,.45),1.5,.08,1)));
	boite->addDrawable(new ShapeDrawable(new Box(Vec3d(0,-1.21,.45),1.5,.08,1)));

	fig->addChild(boite.get());
	fig->addChild(couv.get());
	couv->addChild(couverture.get());

	fig->setAttitude(Quat(PI_2,Vec3(0,0,1)));
	fig->setPosition(Vec3(.6,4.2,0));
	fig->setScale(Vec3(.75,.75,.75));
	//
	//group->addChild(

}
