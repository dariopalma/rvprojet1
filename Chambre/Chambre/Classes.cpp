
#include "Classes.h"
#include "JeuCode.h"
#include "JeuLivres.h"
#include <ctime>
#include <stdio.h>
#include <stdlib.h>

IntersectionSelector::IntersectionSelector(std::vector<Objet*> obj):objets(obj){
	inventory= new objetActuel;
}

Objet::Objet(std::string n,ref_ptr<PositionAttitudeTransform> PATn) : PAT(PATn),nom(n){
	etat[0]=etat[1]=etat[2]=etat[3]=false;
}

void Objet::repositioner(ref_ptr<PositionAttitudeTransform> newPAT){
	this->PAT->setPosition(newPAT->getPosition());
	this->PAT->setAttitude(newPAT->getAttitude());
	this->PAT->setScale(newPAT->getScale());
}

void Tiroir::click(objetActuel* inventory, HUD* hud){
	if(!etat[0])
	{
		afficherMessage(message[0],hud);
		etat[0]=true;
	}
	if(PAT->getPosition().x()==4)
	{
		this->PAT->setPosition(Vec3(3,0,0));
	}
	else
		this->PAT->setPosition(Vec3(4,0,0));
}

Tiroir::Tiroir(std::string name,ref_ptr<PositionAttitudeTransform> pat) : Objet("Tiroir1",pat){

	nom=name;
	message[0] = "Mmmm un tiroir, je vais l'ouvrir";
	etat[0] = 0;
	etat[1] = 0;
	etat[2] = 0;

	ref_ptr<Geode> tiroir1 (new Geode);
	tiroir1->setName("Tiroir1");
	tiroir1->addDrawable(new ShapeDrawable(new Box(Vec3d(1,0,.8),.05,1.5,.3)));
	tiroir1->addDrawable(new ShapeDrawable(new Box(Vec3d(.2,0,.8),.05,1.5,.3)));
	tiroir1->addDrawable(new ShapeDrawable(new Box(Vec3d(.6,0,.675),.8,1.5,.05)));
	tiroir1->addDrawable(new ShapeDrawable(new Box(Vec3d(.6,.75,.8),.85,.05,.3)));
	tiroir1->addDrawable(new ShapeDrawable(new Box(Vec3d(.6,-.75,.8),.85,.05,.3)));

	ref_ptr<Image> image(osgDB::readImageFile("wood.png"));
	ref_ptr<Texture2D> tex(new Texture2D());
	tex->setDataVariance(osg::Object::DYNAMIC);
	tex->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	tex->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	tex->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	tex->setImage(image.get());

	PAT->getOrCreateStateSet()->setTextureAttributeAndModes(0,tex);

	PAT->addChild(tiroir1.get());
}

KeyHoles::KeyHoles(std::string name,ref_ptr<PositionAttitudeTransform> pat):Objet(name,pat.get()){
	message[0]="Un trou pour une cl�";
	message[1]="Cette cle ne rentre pas";
	message[2]="Je n'ai plus besoin d'y toucher";
	message[3]="Ah oui! Ca rentre";
	idCle=-1;//Pas de cle (Que pour le jeu de bombe)
}

void KeyHoles::click(objetActuel* inventory, HUD* hud){
	
	if(!inventory->isNULL())
	{
		std::string aux = inventory->objetPrincip()->getNom();
		aux.erase(aux.begin()+aux.size()-1);
		if(aux=="Cle")
		{
			if(inventory->objetPrincip()->getNom()[3]==nom[7]||this->getNom()[7]=='9')
			{
				afficherMessage(message[3],hud);
				this->etat[2]=true;
				
				inventory->objetPrincip()->setEtat(1,true);
				ref_ptr<PositionAttitudeTransform> aux2(new PositionAttitudeTransform); 
				if(this->getNom()[7]=='9')
				{
					this->idCle=atoi(&inventory->objetPrincip()->getNom()[3]);
					aux2->setPosition(PAT->getPosition()+Vec3(.2,0,0));
					aux2->setScale(Vec3(0.8,0.8,0.8));
					aux2->setAttitude(Quat(PI_2,Vec3(0,1,0)));
				}
				else
				{
					aux2->setPosition(PAT->getPosition()+Vec3(0,.2,0));
					aux2->setScale(Vec3(0.8,0.8,0.8));
					aux2->setAttitude(Quat(-PI_2,Vec3(1,0,0)));
				}
				inventory->popObjetPrincip()->repositioner(aux2.get());
			}
			else
			{
				afficherMessage(message[1],hud);
			}
		}
	}
	else
	{
		afficherMessage(message[0],hud);
	}
}

Bouton::Bouton(std::string name,ref_ptr<PositionAttitudeTransform> pat,JeuCode* j,JeuBombe* j2):Objet(name,pat.get()){
	jeu=j;
	jeuB=j2;
	message[0] = "Un bouton, �a ne bouge pas";
	message[1] = "J'ai appuy� sur le bouton";
	message[2] = "Je n'ai plus besoin d'y toucher";
}

void Bouton::click(objetActuel* inventory, HUD* hud){
	if(jeu!=NULL)
	{
		if(!jeu->codeTrouve())
		{
			if(!etat[2]&&jeu->getObjet('k',this->nom[6])->getEtat(2))
			{
				jeu->resetLEDS();
				this->leverOuAppuyer();
				jeu->ajouterNumero(atoi(&this->nom[6]));
				afficherMessage(message[1],hud);
				if(jeu->tentativeRempli())
				{
					jeu->evaluer();
				}
			}
			else
			{
				afficherMessage(message[0],hud);
			}

		}
		else
		{
			afficherMessage(message[2],hud);
		}
	}
	else
	{
		if(jeuB->getKH()->getIDCLE()>=0)
		{
			if(jeuB->verifierFinJeu())
			{
				hud->start(999999);
				hud->setMessage("I did it!");
			}
			{
				hud->start(0);
				hud->setMessage("Game Over");
			}
		}
		else
		{
			afficherMessage("Il manque une cle",hud);
		}
	}
}

void Bouton::leverOuAppuyer(){
	if(etat[2])
	{
		this->PAT->setPosition(this->PAT->getPosition()+Vec3(0,.03,0));
		etat[2]=false;
	}
	else
	{
		this->PAT->setPosition(this->PAT->getPosition()+Vec3(0,-.03,0));
		etat[2]=true;
	}
}

Cle::Cle(std::string name,ref_ptr<PositionAttitudeTransform> pat,JeuCode* j):Objet(name,pat.get()){
	jeu=j;
	message[0] = "Oh, une cl�";
	message[1] = "Je n'y ai plus besoin de toucher";
}

int Cle::getID(){
	return atoi(&nom[3]);
}

void Cle::click(objetActuel* inventory, HUD* hud){
	if(!etat[0])
	{
		etat[0] = true;
		inventory->ajouterObjet(this);
		afficherMessage(message[0],hud);
		PAT->setScale(PAT->getScale()*.25);
	}
	if(jeu->getObjet('b',this->nom[3])->getEtat(2))
	{
		afficherMessage(message[1],hud);
	}else
		if(etat[1])
		{
			jeu->getObjet('k',this->nom[3])->setEtat(1,false);
			etat[1]=false;
			inventory->ajouterObjet(this);
			afficherMessage(message[0],hud);
			PAT->setScale(PAT->getScale()*.25);
		}
	
}

void Livre::click(objetActuel* inventory, HUD* hud){
	srand(time(NULL));
	int cas;
	if(jeu->getManquant()!=NULL)
	{
		if(inventory->objetPrincip()!=NULL)
		{
			if(inventory->objetPrincip()->getNom()==jeu->getManquant()->getNom())
			{
				cas = 1;//Agregarlo a los livros
			}
			else
			{
				if(this->getID()==jeu->getManquant()->getID())
				{
					cas = 2;//Agregar al inventario
				}
				else
				{
					cas = 3;//Impotencia
				}
			}
		}
		else
		{
			if(this->getID()==jeu->getManquant()->getID())
			{
				cas = 2;
			}
			else
			{
				cas = 3;
			}
		}
	}
	else
	{
		if(jeu->gagnante())
		{
			if(jeu->premierLivreLoin())
			{
				cas = 4;//Arreglados sin hacer nada
			}
			else
			{
				cas = 5;//Esperando a que pique el bueno
			}
		}
		else
		{
			cas = 6;//Cambiando libros
		}
	}

	if(this->getID()==0)
	{
		switch(cas)
		{
		case 1:
			inventory->popObjetPrincip();
			jeu->manquantTrouve();
			break;
		case 3:
			afficherMessage(message[rand()%3],hud);
			break;
		case 5:
			afficherMessage(message[3],hud);
			jeu->retirerPremierLivre();
			break;
		case 6:
			afficherMessage(message[rand()%3],hud);
			break;
		default:
			break;
		}
	}
	else
	{
		switch(cas)
		{
		case 1:
			inventory->popObjetPrincip();
			jeu->manquantTrouve();
			break;
		case 2:
			inventory->ajouterObjet(this);
			afficherMessage("Ce livre me dit quelque chose",hud);
			break;
		case 3:
			afficherMessage(message[rand()%2],hud);
			break;
		case 4:
			afficherMessage(message[2],hud);
			break;
		case 5:
			afficherMessage(message[3],hud);
			break;
		case 6:
			jeu->proposer(this);
		default:
			break;
		}
	}
	
}

void Livre::faireBackUp(){
	reserve = new PositionAttitudeTransform;
	reserve->setPosition(PAT->getPosition());
	reserve->setAttitude(PAT->getAttitude());
	reserve->setScale(PAT->getScale());
}

Livre::Livre(std::string name,ref_ptr<PositionAttitudeTransform> pat,JeuLivres* j,int p):Objet(name,pat.get()){
	jeu=j;
	id=p;
	if(name[5]=='0')
	{
		message[2]="Le livre le plus grand a une marque diff�rente. Mmmm...";
		message[1]="Aurais-je vu ce symbol quelque part?";
		message[0]="Je ne peux pas le d�placer";
		message[3]="Oh! Je peux le d�placer maintenant";
	}
	else
	{
		message[0]="Des livres rouges avec des tailles diff�rentes et une marque bizarre";
		message[1]="Je peux d�placer ce livre mais il manque un autre livre";
		message[3]="Le premier livre a fait un bruit �trange";
		message[2]="Je n'ai plus besoin d'y toucher";
	}
}

Brick::Brick(std::string name, ref_ptr<PositionAttitudeTransform> pat)
: Objet(name,pat.get())
{
	message[0] = "Il est different";
}

void Brick::click(objetActuel* inventory, HUD* hud)
{
	if(!etat[0]){
		afficherMessage(message[0],hud);
		etat[0] = true;
	}else
	{
		this->PAT->setPosition(this->PAT->getPosition()+Vec3d(1,0,0));
	}
};

BrickGame::BrickGame()
{
	float w1 = 0.85, h = 0.35, l = 10, thick = 0.12, w2 = 1.95, delta = 0.01;

	ref_ptr<Geode> brickGeode = new Geode;
	ref_ptr<Image> image(osgDB::readImageFile("whiteBrick.jpg"));
	ref_ptr<Texture2D> tex(new Texture2D());
	tex->setDataVariance(osg::Object::DYNAMIC);
	tex->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	tex->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	tex->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	tex->setImage(image.get());
	brickGeode->getOrCreateStateSet()->setTextureAttributeAndModes(0,tex);

	int distribution[] = {4,6,5,7,6,8,9};

	for (int i = 0; i < 7; ++i)
	{
		if(i%2 == 0)
		{
			for (int j = 0; j < distribution[i]; ++j)
			{
				ref_ptr<ShapeDrawable> shape = new ShapeDrawable(new Box(Vec3d(l/2-thick,-4.5+w1*j+j*delta,l/4+h*i+delta*i),thick,w1,h));
				shape->setName(i+""+j);
				brickGeode->addDrawable( shape.get());
			}
		}else
		{
			for (int j = 0; j < distribution[i]; ++j)
			{
				if(i == 1 && j == 3)
				{
					continue;
				}else
				{
					ref_ptr<ShapeDrawable> 	shape = new ShapeDrawable(new Box(Vec3d(l/2-thick,-4.5+w1*j+j*delta+w1/4,l/4+h*i+delta*i),thick,w1,h));
					brickGeode->addDrawable( shape.get());	
				}
			}
		}
		
	}

	ref_ptr<ShapeDrawable> capsule = new ShapeDrawable(new Capsule(Vec3f(4.88,-3.42,3.58),0.09,0.19));
	capsule->setColor(Vec4(0.1,0.2,0.1,0.0));
	brickGeode->addDrawable( capsule.get());

	ref_ptr<PositionAttitudeTransform> brickPat = new PositionAttitudeTransform;

	_keyBrick = new Brick("brick",brickPat);

	ref_ptr<Geode> brickSpecialGeode = new Geode;
	brickSpecialGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(4.88,-1.7075,2.86),thick,w1,h)));
	brickSpecialGeode->getOrCreateStateSet()->setTextureAttributeAndModes(0,tex);
	brickPat->addChild(brickSpecialGeode.get());
	brickPat->setName("brick");

	_wall = new Group;
	_wall->addChild(brickPat.get());
	_wall->addChild(brickGeode.get());

}

ref_ptr<Group> BrickGame::getFigure()
{
	return _wall;
}

Brick* BrickGame::getBrick()
{
	return _keyBrick;
}

Objet* objetActuel::popObjetPrincip(){
	Objet* aux = objetPrincip();
	eraseObjetPrincip();
	return aux;
}

void objetActuel::ajouterObjet(Objet* obj){
	objets.push_back(obj);
	ref_ptr<PositionAttitudeTransform> aux = obj->getPAT();
	aux->setPosition(aux->getPosition()-Vec3(0,0,-100000));
}

bool objetActuel::isNULL(){
	if(this->objets.size()==0)
		return true;
	else
		return false;
}

void objetActuel::parcourir(int j){
	ref_ptr<PositionAttitudeTransform> aux = objets[i]->getPAT();
	aux->setPosition(aux->getPosition()-Vec3(0,0,-100000));
	if(j=1)
	{
		i++;
		if(i==this->objets.size())
			i=0;
	}
	else
	{
		i--;
		if(i==-1)
			i+=this->objets.size();
	}

	std::cout<<"Objet Actuel: "<<objets[i]->getNom()<<std::endl;
}

std::vector<ref_ptr<PositionAttitudeTransform> > chargerFigures(char* s){
	std::ifstream myFichier(s,std::ios::in);
	std::vector<ref_ptr<PositionAttitudeTransform> > aux;
	if(myFichier)
	{
		int n;
		myFichier>>n;
		ref_ptr<Node> fig;
		StateSet* k;
		for(int i=0;i<n;i++)
		{
			float x,y,z,sx,sy,sz,ax,ay,az;
			std::string h,nombre;
			myFichier>>nombre>>x>>y>>z>>sx>>sy>>sz>>ax>>ay>>az>>h;
			aux.push_back(new PositionAttitudeTransform);
			
			aux[i]->setScale(Vec3(sx,sy,sz));
			aux[i]->setAttitude(Quat(ax*PI/180,Vec3d(1,0,0),ay*PI/180,Vec3d(0,1,0),az*PI/180,Vec3d(0,0,1)));
			aux[i]->setPosition(Vec3(x,y,z));
			fig=osgDB::readNodeFile(h.c_str());
			k = fig->getOrCreateStateSet();
			k->setMode(GL_RESCALE_NORMAL,StateAttribute::ON);
			fig->setStateSet(k);
			aux[i]->addChild(fig.get());
			aux[i]->setName(nombre);
		}
		myFichier.close();
	}
	else
	{
		//hud->afficherMessage("Fichier Introuvable";
	}
	return aux;
}

JeuBombe::JeuBombe(int id) : idCle(id)
{
	JEUFINI = -1;//Pas fini
	jeu = new Group;
	idCle = id;
	ref_ptr<Geode> table(new Geode);
	table->addDrawable(new ShapeDrawable(new Box(Vec3(-4.5,0,1),1,1,1.3)));
	jeu->addChild(table.get());

	//Bombe
	//Seulement si la bombe est l'element 5 du fichier (a compteur depuis 1)
	bombe = new Bombe("Bombe",chargerFigures("../../Models/fichierCharge.txt")[4]);
	jeu->addChild(bombe->getPAT());

	//KeyHole
	ref_ptr<PositionAttitudeTransform> pat = new PositionAttitudeTransform();
	pat->setName("KeyHole9");//9 veut dire pour la bombe
	keyhole = new KeyHoles("KeyHole9",pat.get());
	ref_ptr<Geode> aux(new Geode);
	pat->addChild(aux.get());
	pat->setAttitude(Quat(PI_2,Vec3(0,1,0)));
	pat->setPosition(Vec3(-4.2,0,1));
	aux->addDrawable(new ShapeDrawable(new Cylinder(Vec3(),.05,.5)));
	Vec4 color(0,0,0,0);
	dynamic_cast<ShapeDrawable*>(aux->getDrawable(0))->setColor(color);
	jeu->addChild(pat.get());

	//Bouton
	ref_ptr<PositionAttitudeTransform> pat2 = new PositionAttitudeTransform();
	pat2->setName("Bouton9");
	bouton = new Bouton("Bouton9",pat2.get(),NULL,this);
	ref_ptr<Geode> aux2(new Geode);
	pat2->addChild(aux2.get());
	pat2->setPosition(Vec3(-4,0,.6));
	aux2->addDrawable(new ShapeDrawable(new Box(Vec3(),.05,.05,.05)));
	dynamic_cast<ShapeDrawable*>(aux2->getDrawable(0))->setColor(Vec4(0.5,0.5,0.9,0));
	jeu->addChild(pat2.get());
}

std::vector<Objet*> JeuBombe::getObjets(){
	std::vector<Objet*> aux;
	aux.push_back(bombe);
	aux.push_back(bouton);
	aux.push_back(keyhole);
	return aux;
}
