#pragma once
#include "Classes.h"

class JeuLivres{
private:
	Livre* livres[19];
	bool ordreCorrecte;
	Livre* buffer;
	Livre* manquant;
public:
	bool evaluer();
	Livre* getManquant(){return manquant;}
	void manquantTrouve();
	void proposer(Livre* prop);
	void leverOuReposerLivre(Livre* liv);
	bool premierLivreLoin(){return livres[0]->getEtat(2);}
	void retirerPremierLivre();
	bool gagnante(){return ordreCorrecte;}
	std::vector<Objet*> getLivres();
	void swapLivres(int a, int b);
	JeuLivres();
	void scrambleLivres();
};
