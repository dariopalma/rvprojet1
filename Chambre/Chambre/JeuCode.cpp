#include "JeuCode.h"

JeuCode::JeuCode(){
	correct = false;
	resetTentatives();
	//Pour la choix du code
	srand(time(NULL));
	char aux[10];
	//code[0] = *itoa(rand()%10,aux,10);
	snprintf(aux,10,"%d",rand()%7+1);
	code[0] = aux[0];
	do{
		snprintf(aux,10,"%d",rand()%7+1);
		code[1] = aux[0];
	}while(code[0]==code[1]);
	do{
		snprintf(aux,10,"%d",rand()%7+1);
		code[2] = aux[0];
	}while(code[0]==code[2]||code[1]==code[2]);

	//Inicialisation
	jeu = new Group;

	/////Creation physique
	//Box
	ref_ptr<Geode> shapeGeode(new Geode);
	shapeGeode->addDrawable(new ShapeDrawable(new Box(Vec3d(0,-5,2),3,1,2)));
	ref_ptr<Image> image(osgDB::readImageFile("rusty.jpg"));
	ref_ptr<Texture2D> tex(new Texture2D());
	tex->setDataVariance(osg::Object::DYNAMIC);
	tex->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
	tex->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP);
	tex->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP);
	tex->setImage(image.get());
	shapeGeode->getOrCreateStateSet()->setTextureAttributeAndModes(0,tex);



	for (int i = 0; i < 3; ++i)
	{
		leds[i] = new Geode;
	}
	
	leds[0]->addDrawable(new ShapeDrawable(new Sphere(Vec3(.5,-4.5,2.5),.1)));
	leds[1]->addDrawable(new ShapeDrawable(new Sphere(Vec3(0,-4.5,2.5),.1)));
	leds[2]->addDrawable(new ShapeDrawable(new Sphere(Vec3(-.5,-4.5,2.5),.1)));

	for (int i = 0; i < 3; ++i)
	{
		dynamic_cast<ShapeDrawable*>(leds[i]->getDrawable(0))->setColor(Vec4(1,0,0,0));
	}
	
	std::vector<ref_ptr<PositionAttitudeTransform> > clesCharges = chargerFigures("../../Models/fichierChargeCles.txt");

	//Keyholes et boutons
	for(int i = 0 ; i<7; i++)
	{
		//KeyHoles
		char buffer [30];
		snprintf(buffer,30,"%d",i+1);
		//itoa(i+1,buffer,10);
		std::string h = "KeyHole";
		h+=buffer;
		ref_ptr<PositionAttitudeTransform> pat = new PositionAttitudeTransform();
		pat->setName(h);
		keyHoles[i] = new KeyHoles(h,pat.get());
		ref_ptr<Geode> aux(new Geode);
		pat->addChild(aux.get());
		pat->setAttitude(Quat(PI_2,Vec3(1,0,0)));
		pat->setPosition(Vec3((-.5+.5*i)*(i<3)+(-.75+.5*(i-3))*(i>2),-4.65,2-.5*(i>2)));
		aux->addDrawable(new ShapeDrawable(new Cylinder(Vec3(),.05,.5)));
		Vec4 color(rand()%1001/1000.0,rand()%1001/1000.0,rand()%1001/1000.0,0);
		dynamic_cast<ShapeDrawable*>(aux->getDrawable(0))->setColor(color);
		jeu->addChild(pat.get());

		//Boutons
		h.clear();
		h="Bouton";
		h+=buffer;
		ref_ptr<PositionAttitudeTransform> pat2 = new PositionAttitudeTransform();
		pat2->setName(h);
		bouton[i] = new Bouton(h,pat2.get(),this,NULL);
		ref_ptr<Geode> aux2(new Geode);
		pat2->addChild(aux2.get());
		pat2->setAttitude(Quat(PI_2,Vec3(1,0,0)));
		pat2->setPosition(Vec3((-.5+.5*i)*(i<3)+(-.75+.5*(i-3))*(i>2),-4.48,2-.5*(i>2)-.1));
		aux2->addDrawable(new ShapeDrawable(new Box(Vec3(),.05,.05,.05)));
		dynamic_cast<ShapeDrawable*>(aux2->getDrawable(0))->setColor(Vec4(0.5,0.5,0.9,0));
		jeu->addChild(pat2.get());

		//Cles
		cles[i] = new Cle(clesCharges[i]->getName(),clesCharges[i].get(),this);
		jeu->addChild(clesCharges[i].get());

		ref_ptr<Material> material( new Material);
		material->setDiffuse(Material::FRONT, color);
		material->setColorMode(Material::ColorMode(GL_AMBIENT_AND_DIFFUSE));
		clesCharges[i]->getChild(0)->getOrCreateStateSet()->setAttribute(material.get(),StateAttribute::ON|StateAttribute::OVERRIDE);

	}

	jeu->addChild(shapeGeode.get());
	for (int i = 0; i < 3; ++i)
	{
		jeu->addChild(leds[i]);
	}
}

Objet* JeuCode::getObjet(char type, char num){
	switch(type)
	{
	case 'c':
		return cles[atoi(&num)-1];
		break;
	case 'k':
		return keyHoles[atoi(&num)-1];
		break;
	case 'b':
		return bouton[atoi(&num)-1];
		break;
	default:
		return NULL;
	}
}

void JeuCode::allumerLeds(int fix,int pij)
{
	int i = 0;
	for (i = 0; i < fix; ++i)
	{
		dynamic_cast<ShapeDrawable*>(leds[i]->getDrawable(0))->setColor(Vec4(0,1,0,0));
	}

	for (int j=i; j < i+pij; ++j)
	{
		dynamic_cast<ShapeDrawable*>(leds[j]->getDrawable(0))->setColor(Vec4(1,1,0,0));	
	}

}

void JeuCode::ajouterNumero(int i){

	//********Checar cuando sean iguales******

	if(tentative[0]=='0')
	{
		tentative[0]=i+'0';
	}else
		if(tentative[1]=='0')
		{
			tentative[1]=i+'0';
		}else
			if(tentative[2]=='0')
			{
				tentative[2]=i+'0';
			}
			else
			{
				////
			}
}

void JeuCode::evaluer(){
	int fix,pic;
	fix=pic=0;
	if(code[0]==tentative[0])
		fix++;
	if(code[1]==tentative[1])
		fix++;
	if(code[2]==tentative[2])
		fix++;
	if(code[0]==tentative[1]||code[0]==tentative[2])
		pic++;
	if(code[1]==tentative[2]||code[1]==tentative[0])
		pic++;
	if(code[2]==tentative[0]||code[2]==tentative[1])
		pic++;
	allumerLeds(fix,pic);
	if(fix==3)
	{
		correct = true;
	}
	else
		resetJeu();
}

void JeuCode::resetJeu(){
	resetTentatives();
	
	for(int i = 0; i<7; i++)
	{
		if(bouton[i]->getEtat(2))
			bouton[i]->leverOuAppuyer();
	}
}

bool JeuCode::tentativeRempli(){
	if(tentative[2]=='0')
		return false;
	else
		return true;
}

void JeuCode::resetTentatives(){
	tentative[0]=tentative[1]=tentative[2]='0';
}

void JeuCode::resetLEDS()
{
	for (int i = 0; i < 3; ++i)
	{
		dynamic_cast<ShapeDrawable*>(leds[i]->getDrawable(0))->setColor(Vec4(1,0,0,0));
	}
}

std::vector<Objet*> JeuCode::getClesEtKeyholes(){
	std::vector<Objet*> aux;
	for(int i=0; i<7; i++)
	{
		aux.push_back(cles[i]);
		aux.push_back(keyHoles[i]);
		aux.push_back(bouton[i]);
	}
	return aux;
}

int JeuCode::getIDbonneCle(){
	int a;
	do{
		a=rand()%7+1;
	}while(a==atoi(&code[0])||a==atoi(&code[1])||a==atoi(&code[2]));
	return a;
}