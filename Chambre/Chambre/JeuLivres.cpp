#include "JeuLivres.h"

JeuLivres::JeuLivres(){
	int randAux=rand()%17+1;
	buffer = NULL;
	ordreCorrecte=false;
	ref_ptr<PositionAttitudeTransform> auxM = new PositionAttitudeTransform;
	std::vector<ref_ptr<PositionAttitudeTransform> > auxCharge = chargerFigures("../../Models/fichierChargeLivres.txt");
	livres[0]=new Livre("Livre0",auxM.get(),this,0);
	auxM->setName("Livre0");
	ref_ptr<Geode> pillule(new Geode);
	
	pillule->addDrawable(new ShapeDrawable(new Capsule(Vec3(2.1,4.55,1.75),.02,.05)));
	auxM->addChild(auxCharge[0].get());
	auxM->addChild(pillule.get());
	dynamic_cast<ShapeDrawable*>(pillule->getDrawable(0))->setColor(Vec4(0.1,0.2,0.1,0));
	for(int i = 1; i<19; i++)
	{
		char buffer [30];
		snprintf(buffer,30,"%d",i);
		std::string h = "Livre";
		h+=buffer;
		ref_ptr<PositionAttitudeTransform> aux1 = new PositionAttitudeTransform;
		ref_ptr<PositionAttitudeTransform> aux2 = new PositionAttitudeTransform;
		ref_ptr<PositionAttitudeTransform> aux3 = new PositionAttitudeTransform;
		ref_ptr<Geode> sphere(new Geode);
		sphere->addDrawable(new ShapeDrawable(new Sphere(Vec3(2.1,4.55,1.6),.02)));
		aux2->setName(h.c_str());
		aux1->addChild(auxCharge[0].get());
		aux2->addChild(aux1.get());
		aux2->addChild(aux3.get());
		aux3->addChild(sphere.get());
		aux3->setPosition(Vec3(i/10.0,0,0));
		aux1->setScale(Vec3(1,1,(30-i)/30.0));
		aux1->setPosition(Vec3(i/10.0,0,1.5*i/30.0));
		livres[i]=new Livre(h,aux2.get(),this,i);
		switch(i){
		case 1:
		case 7:
		case 8:
		case 9:
			//Nord
			dynamic_cast<ShapeDrawable*>(sphere->getDrawable(0))->setColor(Vec4(1,1,0,0));
			break;
		case 2:
		case 10:
		case 17:
			//Est
			dynamic_cast<ShapeDrawable*>(sphere->getDrawable(0))->setColor(Vec4(1,.6,0,0));
			break;
		case 3:
		case 4:
		case 11:
		case 13:
		case 16:
		case 18:
			//Sud
			dynamic_cast<ShapeDrawable*>(sphere->getDrawable(0))->setColor(Vec4(.7,.7,1,0));
			break;
		default:
			//Ouest
			dynamic_cast<ShapeDrawable*>(sphere->getDrawable(0))->setColor(Vec4(1,.5,.5,0));
		}
	}
	
	scrambleLivres();
	manquant = livres[randAux];
	livres[randAux]->faireBackUp();
}

void JeuLivres::scrambleLivres(){
	srand(time(NULL));
	for(int i = 1;i<18;i++)
	{
		int aux = rand()%(18-i)+i;
		swapLivres(i,aux);
	}
}

void JeuLivres::retirerPremierLivre(){
	this->livres[0]->transformPosit(Vec3(0,0,-999999));
	livres[0]->setEtat(2,true);
}

std::vector<Objet*> JeuLivres::getLivres(){
	std::vector<Objet*> aux;
	for(int i=0;i<19;i++)
	{
		aux.push_back(livres[i]);
	}
	return aux;
}

void JeuLivres::swapLivres(int a, int b){
	Livre* aux = livres[a];
	livres[a]->transformPosit(livres[a]->getPAT()->getPosition()+Vec3((b-a)/10.0,0,0));
	livres[b]->transformPosit(livres[b]->getPAT()->getPosition()-Vec3((b-a)/10.0,0,0));
	livres[a]->setID(b);
	livres[b]->setID(a);
	livres[a]=livres[b];
	livres[b]=aux;
}

void JeuLivres::proposer(Livre* prop){
	if(buffer!=NULL)
	{
		leverOuReposerLivre(NULL);
		swapLivres(buffer->getID(),prop->getID());
		evaluer();
		buffer=NULL;
	}
	else
	{
		buffer=prop;
		leverOuReposerLivre(buffer);
	}
}

void JeuLivres::manquantTrouve(){
	manquant->repositioner(manquant->greserve());
	manquant=NULL;
}

void JeuLivres::leverOuReposerLivre(Livre* liv){
	if(liv==NULL)
	{
		buffer->transformPosit(buffer->getPAT()->getPosition()-Vec3(0,0,.25));
	}
	else
	{
		liv->transformPosit(liv->getPAT()->getPosition()+Vec3(0,0,.25));
	}
}

bool JeuLivres::evaluer(){
	for(int i=0;i<18;i++)
	{
		if(livres[i]->getID()!=atoi(&livres[i]->getNom()[5]))
		{
			return false;
		}
	}
	this->ordreCorrecte=true;
	return true;
}
