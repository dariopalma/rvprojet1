#pragma once

#ifdef WIN32
#define snprintf _snprintf
#define PATHLEVEL "(cd ../../GUI/; ./gameOver.exe)"
#else
#define PATHLEVEL "(cd ../../GUI/; ./gameOver)"
#endif

#include <osgViewer/Viewer>
#include <osg/PositionAttitudeTransform>
#include <osgText/Text>
#include <osg/Drawable>
#include <osg/ShapeDrawable>
#include <osg/Texture2D>
#include <osg/Material>
#include <iostream>
#include <osgDB/ReadFile>

using namespace osg;

class objetActuel;

class HUD 
{
private:
	unsigned int _limit;
	ref_ptr<Camera> _camera;
	unsigned int _winX, _winY;
	Vec3 _clockPosition;
	Vec3 _MessagePosition;

public:
	virtual void click(objetActuel* inventory, HUD* hud){
		this->setMessage("");
	}
	ref_ptr<osgText::Text> _clock;
	ref_ptr<osgText::Text> _Message;

	class HUDCB : public osg::NodeCallback
	{
	private:
		HUD* _hud;

	public:
		HUDCB(HUD* hud)
		{
			_hud = hud;
		}
		virtual void operator()(osg::Node* node, osg::NodeVisitor* nv)
		{
			_hud->_clock->setText(_hud->timeLeftString());
			if(_hud->timeLeft() < 0)
			{
				std::system(PATHLEVEL);
				exit(0);
			}
			traverse(node,nv);
		}
	};

	void start(int limit)
	{
		_limit = (unsigned int) limit;
	}

	int timeLeft()
	{
		return _limit-(int)clock()/CLOCKS_PER_SEC;
	}

	std::string timeLeftString()
	{
		std::stringstream ss;
		int minutes = this->timeLeft()/60;
		int seconds = this->timeLeft()%60;
		ss<< minutes << " : " << seconds;
		return ss.str();
	}

	bool timesUP()
	{
		return _limit > (unsigned int) clock()/CLOCKS_PER_SEC;
	}

	void setMessage(const char* s)
	{
		std::string st = s;
		int length = st.length();
		_MessagePosition = Vec3(_winX/2-length/2*24, _winY/2, 0.0f);
		_Message->setPosition(_MessagePosition);
		_Message->setText(s);
	}

	Camera* createHUD() {

		GraphicsContext::WindowingSystemInterface* wsi = GraphicsContext::getWindowingSystemInterface();
		wsi->getScreenResolution(GraphicsContext::ScreenIdentifier(0),_winX,_winY);

		_camera = new Camera;
		_camera->setClearMask( GL_DEPTH_BUFFER_BIT );
		_camera->setRenderOrder( osg::Camera::POST_RENDER );
		_camera->setReferenceFrame( osg::Camera::ABSOLUTE_RF );
		_camera->setProjectionMatrix( osg::Matrix::ortho2D(0,_winX,0,_winY));
		_camera->setViewMatrix(osg::Matrix::identity());
		_camera->setAllowEventFocus(false);

		ref_ptr<Geode> geode = new Geode();
		geode->setName("Message");
		ref_ptr<StateSet> statesetG = geode->getOrCreateStateSet();
		statesetG->setMode(GL_LIGHTING, StateAttribute::OFF);
		statesetG->setDataVariance( Object::DYNAMIC);
	
		_clock = new osgText::Text;
		_Message = new osgText::Text;
		geode->addDrawable( _clock.get());
		geode->addDrawable( _Message.get());

		_clockPosition = Vec3(_winX/2-3*24, _winY-100.0f, 0.0f);
		_MessagePosition = Vec3(_winX/2, _winY/2, 0.0f);

		this->start(10*60);

		_clock->setText("");
		_clock->setPosition(_clockPosition);

		this->setMessage("No time to lose");

		_camera->setUpdateCallback( new HUDCB(this));

		_camera->addChild( geode.get());

		return _camera;
	}

};

class Objet 
{
protected:
	ref_ptr<PositionAttitudeTransform> PAT;
	std::string nom;
	std::string message[4];
	
	bool etat[3];//Investigado, Tomado, Usado
public:
	virtual std::string getType() = 0;
	void afficherMessage(std::string s,HUD* hud){hud->setMessage(s.c_str());}
	void setEtat(int i, bool val){etat[i] = val;}
	bool getEtat(int i){return etat[i];}
	void repositioner(ref_ptr<PositionAttitudeTransform> newPAT);
	ref_ptr<PositionAttitudeTransform> getPAT() {return PAT.get();}
	virtual void click(objetActuel* inventory,HUD* hud) = 0;
	std::string getNom(){return nom;}
	void transformPosit(Vec3 vec){PAT->setPosition(vec);}
	void transformAttit(Quat quat){PAT->setAttitude(quat);}
	void transformScale(Vec3 vec){PAT->setScale(vec);}
	Objet(std::string n,ref_ptr<PositionAttitudeTransform> PATn);
};

class Tiroir : public Objet
{
public:
	virtual std::string getType(){return "Tiroir";};
	virtual void click(objetActuel* inventory, HUD* hud);
	Tiroir(std::string name, ref_ptr<PositionAttitudeTransform> pat);
};

class Boite : public Objet
{
public:
	virtual std::string getType(){return "Boite";};
	virtual void click(objetActuel* inventory, HUD* hud){afficherMessage("Une boite",hud);};
	Boite(std::string name,ref_ptr<PositionAttitudeTransform> pat):Objet(name,pat.get()){nom=name;}
};

class Cheminee : public Objet
{
public:
	virtual std::string getType(){return "Cheminee";};
	virtual void click(objetActuel* inventory, HUD* hud){afficherMessage("Une cheminee",hud);};
	Cheminee(std::string name,ref_ptr<PositionAttitudeTransform> pat):Objet(name,pat.get()){nom=name;}
};

class Pot : public Objet
{
public:
	virtual std::string getType(){return "Pot";};
	virtual void click(objetActuel* inventory, HUD* hud){afficherMessage("Un Pot",hud);};
	Pot(std::string name,ref_ptr<PositionAttitudeTransform> pat):Objet(name,pat.get()){nom=name;}
};

class Arbre : public Objet
{
public:
	virtual std::string getType(){return "Arbre";};
	virtual void click(objetActuel* inventory, HUD* hud){afficherMessage("Un arbre mort",hud);};
	Arbre(std::string name,ref_ptr<PositionAttitudeTransform> pat):Objet(name,pat.get()){nom=name;}
};

class JeuLivres;

class Livre : public Objet
{
private:
	JeuLivres* jeu;
	ref_ptr<PositionAttitudeTransform> reserve;
	int id;
public:
	void faireBackUp();
	ref_ptr<PositionAttitudeTransform> greserve(){return reserve.get();}
	virtual std::string getType(){return "Livre";}
	void setID(int h){id=h;}
	float getScaleZ(){return this->PAT->getScale().z();}
	int getID(){return id;}
	virtual void click(objetActuel* inventory, HUD* hud);
	Livre(std::string name,ref_ptr<PositionAttitudeTransform> pat,JeuLivres* j,int p);
};

class KeyHoles : public Objet
{
private:
	int idCle;
public:
	void setIDCle(int h){idCle = h;};
	int getIDCLE(){return idCle;}
	virtual std::string getType(){return "KeyHole";};
	virtual void click(objetActuel* inventory, HUD* hud);
	KeyHoles(std::string name,ref_ptr<PositionAttitudeTransform> pat);
};

class JeuCode;
class JeuBombe;

class Bouton: public Objet
{
private:
	JeuCode* jeu;
	JeuBombe* jeuB;
public:
	virtual std::string getType(){return "Bouton";};
	void leverOuAppuyer();
	virtual void click(objetActuel* inventory, HUD* hud);
	Bouton(std::string name,ref_ptr<PositionAttitudeTransform> pat,JeuCode* j,JeuBombe* j2);
};

class Cle : public Objet
{
private : 
	JeuCode* jeu;
public :
	virtual std::string getType(){return "Cle";}
	virtual void click(objetActuel* inventory, HUD* hud);
	Cle(std::string name,ref_ptr<PositionAttitudeTransform> pat,JeuCode* j);
	int getID();
};

class Brick : public Objet
{
public:
	virtual std::string getType(){return "Brick";};
	virtual void click(objetActuel* inventory, HUD* hud);
	Brick(std::string name, ref_ptr<PositionAttitudeTransform> pat);
};

class BrickGame
{
private:
	ref_ptr<Group> _wall;
	Brick* _keyBrick;
	Cle* _cleFinale;
public:
	BrickGame();
	ref_ptr<Group> getFigure();
	Brick* getBrick();
};

class Bombe : public Objet
{
public:
	virtual void click(objetActuel* inventory, HUD* hud){afficherMessage("Je suis trop jeune pour mourir",hud);}
	virtual std::string getType(){return "Bombe";}
	Bombe(std::string name, ref_ptr<PositionAttitudeTransform> patN):Objet(name,patN){}
};

class JeuBombe{
private:
	ref_ptr<Group> jeu;
	int JEUFINI;
	Bombe* bombe;
	Bouton* bouton;
	int idCle;
	KeyHoles* keyhole;
public:
	KeyHoles* getKH(){return keyhole;}
	JeuBombe(int id);
	bool verifierFinJeu(){JEUFINI= keyhole->getIDCLE()==idCle;return JEUFINI;}
	std::vector<Objet*> getObjets();
	ref_ptr<Group> getGroup(){return jeu.get();}
};

class Lock : public Objet
{
private:
	int code[5];
public:
	virtual void click(objetActuel* inventory, HUD* hud){}
	virtual std::string getType(){return "Lock";}
	Lock(std::string name, ref_ptr<PositionAttitudeTransform> patN):Objet(name,patN){}
};

//Inventory
class objetActuel{
private:
	std::vector<Objet*> objets;//Objets en possession
	int i;//Index de l'objet;
	void eraseObjetPrincip(){objets.erase(objets.begin()+i);i=0;}
public:
	objetActuel(){i = 0;}
	bool isNULL();
	void parcourir(int j);
	void ajouterObjet(Objet* obj);
	Objet* objetPrincip(){if(isNULL())return NULL; else return objets[i];}
	Objet* popObjetPrincip();//Effacer et Obtenir
};

class IntersectionSelector
{
private:
	std::vector<Objet*> objets;
	objetActuel* inventory;
public:
	objetActuel* getInventory(){return inventory;}
	void parcourirInventory(int l){inventory->parcourir(l);}
	virtual bool handle(osgUtil::LineSegmentIntersector::Intersection &intersection,HUD* hud)
	{
		int b = 0,p=0;
		NodePath &nodes = intersection.nodePath;
		for(int i=nodes.size()-1;i>=0;i--)
		{
			if(findObjet(nodes[i]->getName()) != NULL)
			{
				if(nodes[i]->getName()=="Livre0")
					b=i;
				else
				{
					findObjet(nodes[i]->getName())->click(inventory,hud);
					p=1;
				}
			}
		}
		if(!p&&b)
			findObjet(nodes[b]->getName())->click(inventory,hud);

		return false;
	}

	Objet* findObjet(std::string name){
		for(int i = 0;i<objets.size();i++)
		{
			if(objets[i]->getNom()==name)
				return objets[i];
		}
		return NULL;
	}

	IntersectionSelector(std::vector<Objet*> obj);
};

std::vector<ref_ptr<PositionAttitudeTransform> > chargerFigures(char* s);


