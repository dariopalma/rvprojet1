#pragma once
#include "Classes.h"

class JeuLock
{
private:
	Lock* lock;
	char code[5];
	ref_ptr<PositionAttitudeTransform> couv;
	ref_ptr<PositionAttitudeTransform> fig;
public:
	JeuLock();
	void animationOuvrir();
	ref_ptr<PositionAttitudeTransform> getFigure(){return fig.get();}
};
