/**********************************************************************************/
/*                                   Visualizer                                   */
/*                               /autor dariopalma                                */
/**********************************************************************************/

#include <osg/ArgumentParser>
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/ShapeDrawable>
#include <osgText/Text>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>
#include <osg/PositionAttitudeTransform>

class ScaleNode : public osgGA::GUIEventHandler
{
private:
	float _scale;
	float _scaleDelta;
	osg::PositionAttitudeTransform* _pat;
	osgText::Text* _scaleText;

public:
	ScaleNode(osgText::Text* text, osg::PositionAttitudeTransform* pat) : osgGA::GUIEventHandler()
	{	
		_scale = 1.0;
		_scaleText = text;
		_pat = pat;
		_scaleDelta = 0.01;
	}

	bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us)
	{
		switch(ea.getKey())
		{
			case osgGA::GUIEventAdapter::KEY_Down:
				_scale -= _scaleDelta;
				_pat->setScale(osg::Vec3(_scale,_scale,_scale));
				break;
			case osgGA::GUIEventAdapter::KEY_Up:
				_scale += _scaleDelta;
				_pat->setScale(osg::Vec3(_scale,_scale,_scale));
				break;
			case osgGA::GUIEventAdapter::KEY_Left:
				_scaleDelta /= 10;
				break;
			case osgGA::GUIEventAdapter::KEY_Right:
				_scaleDelta *= 10;
				break;
		}

		std::cout << "scale : " << _scale << " scaleDelta : " << _scaleDelta << std::endl;

		return false;
	}
};



/* This program allows for a quick lecture check of a provided object.
   As first tool we can see if the object loads well in OSG and as 
   second utility we can draw a Cube of different dimensions to look at
   the scale of our model in OSG.

   Example usage arguments:

	../Models/Door/Door.obj --DrawCube -100

	This allows us to see the origin and relative dimensions.

	Also we can scale the object and see the current scale value at the console

	Arrows UP DOWN scale the object : +scale -scale
	LEFT RIGHT change the scaling parameter : /10 and *10 */

int main( int argc, char **argv)
{
	osg::ArgumentParser arguments(&argc,argv);

	osg::ref_ptr<osg::Group> scene = new osg::Group;

	osg::ref_ptr<osg::Node> file = osgDB::readNodeFiles(arguments);



	// In case of error drop
	if(!file)
	{
		osg::notify(osg::NOTICE)<<"No file loaded"<<std::endl;
		return 1;
	}

	// Draw cube with determined dimensions
	if(arguments.read("--DrawCube"))
	{
		osg::ref_ptr<osg::Geode> geode = new osg::Geode;
		osg::ref_ptr<osg::Box> box;
		if(arguments.read("-1")) box = new osg::Box(osg::Vec3(),1);
		if(arguments.read("-10")) box = new osg::Box(osg::Vec3(),10);
		if(arguments.read("-100")) box = new osg::Box(osg::Vec3(),100);
		else  box = new osg::Box(osg::Vec3(),1);

		osg::ref_ptr<osg::ShapeDrawable> drawable = new osg::ShapeDrawable(box.get());

		geode->addDrawable(drawable.get());

		scene->addChild(geode.get());
	}

	osg::ref_ptr<osg::PositionAttitudeTransform> PAT = new osg::PositionAttitudeTransform;

	PAT->addChild(file.get());

	scene->addChild(PAT.get());

	osgViewer::Viewer viewer;

	viewer.setSceneData(scene.get());

	viewer.addEventHandler(new ScaleNode(NULL,PAT.get()));

	return viewer.run();
}