cmake_minimum_required(VERSION 3.5)

SET(PROJECT_NAME visualizer)

PROJECT(${PROJECT_NAME})

FIND_PACKAGE(osg)
FIND_PACKAGE(osgUtil)
FIND_PACKAGE(osgViewer)
FIND_PACKAGE(osgDB)
FIND_PACKAGE(osgText)
FIND_PACKAGE(osgGA)

SET(SOURCES
visualizer.cpp
)

INCLUDE_DIRECTORIES(${OSG_INCLUDE_DIR} )

LINK_DIRECTORIES(${OSG_LIB_DIR})

add_executable(${PROJECT_NAME} ${SOURCES})

TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${OSG_LIBRARY} ${OSGUTIL_LIBRARY} ${OSGVIEWER_LIBRARY} ${OSGDB_LIBRARY}  ${OSGGA_LIBRARY} ${OSGTEXT_LIBRARY}) 
