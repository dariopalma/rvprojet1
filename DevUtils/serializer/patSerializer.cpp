/**********************************************************************************/
/*                                   Visualizer                                   */
/*                               /autor dariopalma                                */
/**********************************************************************************/

#include <osg/Node>
#include <osgDB/ObjectWrapper>
#include <osgDB/Registry>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>
#include <osg/PositionAttitudeTransform>
#include <iostream>
#include <string>

/* This program scales the given object given the number and saves it in a native osg
   node format */

int main(int argc, char** argv)
{
	osg::ArgumentParser arguments( &argc, argv );
	
	if(arguments.argc()==1)
	{
		std::cout << "No arguments given" << std::endl;
		return 1;
	}
	else if(arguments.argc()!=5)
	{
		std::cout << "Bad arguments" << std::endl;
		std::cout << "Use : originalfile.extension newname scale" << std::endl;
		std::cout << "Example : -s file.stl fileInOSG 20" << std::endl;
		std::cout <<arguments.argc();
		return 1;
	}

	double scale;

	std::string fileName;
	std::string osgName;

	arguments.read("-s",fileName,osgName,scale);
	
	osg::PositionAttitudeTransform* PAT = new osg::PositionAttitudeTransform;
	PAT->setScale(osg::Vec3(scale,scale,scale));
	osg::Node* file = osgDB::readNodeFile( fileName);
	PAT->addChild(file);

	osgDB::writeNodeFile( *PAT, std::string(osgName+".osgt"));

	return 0;
}